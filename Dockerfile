FROM tiangolo/node-frontend:10 as build-stage

ARG stage
ARG prod
ENV DEXTER_FRONTEND_STAG=$stage
ENV DEXTER_FRONTEND_PROD=$prod

WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
RUN yarn build

FROM nginx:latest
COPY --from=build-stage /app/build/ /usr/share/nginx/html