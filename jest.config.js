module.exports = {
  testMatch: ['**/__tests__/**/*_Spec.js'],
  collectCoverage: true,
  collectCoverageFrom: ['lib/js/src/**/!(*_Style).js'],
  snapshotResolver: '<rootDir>/snapshotResolver.js',
  globals: {
    PROD: false,
    STAG: false,
    VERSION: 'TEST'
  }
}
