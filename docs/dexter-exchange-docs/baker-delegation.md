# Baker Delegation

XTZ held by the Dexter contracts can be delegated to bakers. The UI displays
the address of the Baker to which a particular Dexter contract has delegated
its XTZ to. The choice of baker is controlled by the manager contract. Currently
all Dexter contracts originated by camlCase and displayed on dexter.exchange are
managed by a camlCase multisig contract. camlCase delegates to trusted bakers.

In order to ensure that liquidity providers receive their awards even if they 
remove their liquidity from a Dexter contract, we delegate only to bakers that
use the tzpay tool. tzpay has a special set of baker logic that allows the baker
to pay rewards directly to the liquidity provides. 

The rewards are not paid to the dexter contract directly. This would allow new 
liquidity providers to get rewards they did not earn and previous providers would
miss out on rewards they earned for previous cycles.

## Technical Details
