open Jest;
open Expect;

let fp = "__tests__/golden/";

describe("parsing dexter data", () => {
  test("storage", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(fp ++ "dexter_storage.json"),
      );
    let rExpression = Tezos.Expression.decode(json);

    expect(Belt.Result.isOk(rExpression)) |> toEqual(true) |> ignore;

    switch (rExpression) {
    | Ok(expression) =>
      {
        expect(
          Belt.Result.isOk(Dexter.Store.ofExpression(4,expression)),
        );
      }
      |> toEqual(true)
    | _ => expect(false) |> toEqual(true)
    };
  });

  test("big map", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(fp ++ "dexter_bigmap.json"),
      );
    let rExpression = Tezos.Expression.decode(json);

    expect(Belt.Result.isOk(rExpression)) |> toEqual(true) |> ignore;

    switch (rExpression) {
    | Ok(expression) =>
      expect(Belt.Result.isOk(Dexter.BigMap.ofExpression(expression)))
      |> toEqual(true)
    | _ => expect(false) |> toEqual(true)
    };
  });
});
