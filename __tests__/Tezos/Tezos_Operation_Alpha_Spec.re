open Jest;
open Expect;

describe("Tezos_Operation_Alpha.Internal.Operation.Result.decode", () => {
  test("golden", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(
          "__tests__/golden/internal_operation_result.json",
        ),
      );
    expect(
      Tezos_Operation_Alpha.Internal.Operation.Result.decode(json)
      |> Belt.Result.isOk,
    )
    |> toEqual(true);
  })
});

describe("Tezos_Operation_Alpha.Contents.Result.Transaction.decode", () => {
  test("golden", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(
          "__tests__/golden/operation_content_result_transaction.json",
        ),
      );
    expect(
      Tezos_Operation_Alpha.Contents.Result.Transaction.decode(json)
      |> Belt.Result.isOk,
    )
    |> toEqual(true);
  })
});

describe(
  "Tezos_Operation_Alpha.Contents.Result.Transaction.Metadata.decode", () => {
  test("golden", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(
          "__tests__/golden/operation_content_result_transaction_metadata.json",
        ),
      );
    Js.log(
      Tezos_Operation_Alpha.Contents.Result.Transaction.Metadata.decode(json),
    );
    expect(
      Tezos_Operation_Alpha.Contents.Result.Transaction.Metadata.decode(json)
      |> Belt.Result.isOk,
    )
    |> toEqual(true);
  })
});

describe("Tezos_Operation_Alpha.BalanceUpdates.decode", () => {
  test("golden", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync("__tests__/golden/balance_updates.json"),
      );
    expect(
      Tezos_Operation_Alpha.BalanceUpdate.decode(json) |> Belt.Result.isOk,
    )
    |> toEqual(true);
  })
});

describe("Tezos_Operation_Alpha.Operation.Result.Transaction.decode", () => {
  test("golden", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(
          "__tests__/golden/operation_result_transaction.json",
        ),
      );
    expect(
      Tezos_Operation_Alpha.Operation.Result.Transaction.decode(json)
      |> Belt.Result.isOk,
    )
    |> toEqual(true);
  })
});

describe("Tezos_Operation_Alpha.Parameters.decode", () => {
  test("golden", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync("__tests__/golden/parameters.json"),
      );
    expect(Tezos_Operation_Alpha.Parameters.decode(json))
    |> toEqual(
         Belt.Result.Ok(
           {
             entrypoint: Tezos_Operation_Alpha.Entrypoint.Named("xtzToToken"),
             value:
               Tezos_Expression.(
                 SingleExpression(
                   Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
                   Some([
                     StringExpression("tz1gV3LBv1XmVEkdeRTjuUdurvCf8P85eALM"),
                     SingleExpression(
                       Tezos_Primitives.PrimitiveData(
                         Tezos_PrimitiveData.Pair,
                       ),
                       Some([
                         IntExpression(Bigint.of_int(18440679)),
                         StringExpression("2020-10-11T01:20:22.807Z"),
                       ]),
                       None,
                     ),
                   ]),
                   None,
                 )
               ),
           }: Tezos_Operation_Alpha.Parameters.t,
         ),
       );
  });

  test("golden", () => {
    let json =
      Js.Json.parseExn(
        Node.Fs.readFileAsUtf8Sync(
          "__tests__/golden/parameters_as_string.json",
        ),
      );
    Js.log(Js.Json.string(Json.Decode.string(json)));
    expect(
      Tezos_Operation_Alpha.Parameters.decode(
        Js.Json.parseExn(Json.Decode.string(json)),
      ),
    )
    |> toEqual(
         Belt.Result.Ok(
           {
             entrypoint: Tezos_Operation_Alpha.Entrypoint.Named("xtzToToken"),
             value:
               Tezos_Expression.(
                 SingleExpression(
                   Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
                   Some([
                     StringExpression("tz1gV3LBv1XmVEkdeRTjuUdurvCf8P85eALM"),
                     SingleExpression(
                       Tezos_Primitives.PrimitiveData(
                         Tezos_PrimitiveData.Pair,
                       ),
                       Some([
                         IntExpression(Bigint.of_int(18440679)),
                         StringExpression("2020-10-11T01:20:22.807Z"),
                       ]),
                       None,
                     ),
                   ]),
                   None,
                 )
               ),
           }: Tezos_Operation_Alpha.Parameters.t,
         ),
       );
  });
});
