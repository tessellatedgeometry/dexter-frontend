open Jest;
open Expect;

describe("getDecimalsCountInString", () => {
  test("10, 0", () =>
    expect(Utils.getDecimalsCountInString("10")) |> toEqual(0)
  );
  test("10.10, 2", () =>
    expect(Utils.getDecimalsCountInString("10.10")) |> toEqual(2)
  );
  test("10.101010, 6", () =>
    expect(Utils.getDecimalsCountInString("10.101010")) |> toEqual(6)
  );
});

describe("omitIpfsInPath", () => {
  test("['ipns', 'QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7'], []", () =>
    expect(
      Utils.omitIpfsInPath([
        "ipns",
        "QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7",
      ]),
    )
    |> toEqual([])
  );
  test(
    "['ipfs', 'QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7', 'liquidity', 'add'], ['liquidity', 'add']",
    () =>
    expect(
      Utils.omitIpfsInPath([
        "ipfs",
        "QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7",
        "liquidity",
        "add",
      ]),
    )
    |> toEqual(["liquidity", "add"])
  );
  test("['liquidity', 'add'], ['liquidity', 'add']", () =>
    expect(Utils.omitIpfsInPath(["liquidity", "add"]))
    |> toEqual(["liquidity", "add"])
  );
});

describe("getIpfsBaseUrlFromPath", () => {
  test("['ipns', 'QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7'], []", () =>
    expect(
      Utils.getIpfsBaseUrlFromPath([
        "ipns",
        "QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7",
      ]),
    )
    |> toEqual("/ipns/QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7/")
  );
  test(
    "['ipfs', 'QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7', 'liquidity', 'add'], ['liquidity', 'add']",
    () =>
    expect(
      Utils.getIpfsBaseUrlFromPath([
        "ipfs",
        "QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7",
        "liquidity",
        "add",
      ]),
    )
    |> toEqual("/ipfs/QmSesnsoCJr8sQd6wuPHsCEUn6eyVz5wZ12rKWTNQJLki7/")
  );
  test("['liquidity', 'add'], ['liquidity', 'add']", () =>
    expect(Utils.getIpfsBaseUrlFromPath(["liquidity", "add"]))
    |> toEqual("/")
  );
});
