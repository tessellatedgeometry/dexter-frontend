open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Renders", () => {
    render(<DexterUi />) |> container |> expect |> toMatchSnapshot
  })
});
