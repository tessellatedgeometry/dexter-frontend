open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("No account", () => {
    render(<DexterUi_Transactions />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Transactions tab", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          account: Some(Fixtures_Account.account1),
          transactions: Some([]),
        }>
        <DexterUi_Transactions />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("PoolTokens tab", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          account: Some(Fixtures_Account.account1),
        }>
        <DexterUi_Transactions defaultTab=PoolTokens />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
