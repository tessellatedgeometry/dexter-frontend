open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Light mode", () => {
    render(
      <DexterUi_Context.ProviderInternal value=DexterUi_Context.defaultValue>
        <DexterUi_Icon name="exchange" />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Dark mode; No darkModeSuffix", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={...DexterUi_Context.defaultValue, darkMode: true}>
        <DexterUi_Icon size=15 name="exchange" />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Light mode; With darkModeSuffix", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={...DexterUi_Context.defaultValue, darkMode: true}>
        <DexterUi_Icon size=15 name="exchange" darkModeSuffix="-d" />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
