open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Light mode; No onClick", () => {
    render(
      <DexterUi_Context.ProviderInternal value=DexterUi_Context.defaultValue>
        <DexterUi_ExchangeSpacer />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Dark mode; No onClick", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={...DexterUi_Context.defaultValue, darkMode: true}>
        <DexterUi_ExchangeSpacer />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Light mode; With onClick", () => {
    render(
      <DexterUi_Context.ProviderInternal value=DexterUi_Context.defaultValue>
        <DexterUi_ExchangeSpacer onClick={_ => ()} />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Dark mode; With onClick", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={...DexterUi_Context.defaultValue, darkMode: true}>
        <DexterUi_ExchangeSpacer onClick={_ => ()} />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
