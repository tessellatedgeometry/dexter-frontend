open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Exchange", () => {
    render(<DexterUi_TransactionIcon transactionType=Exchange />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Exchange; Disabled", () => {
    render(
      <DexterUi_TransactionIcon transactionType=Exchange disabled=true />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("ExchangeAndSend", () => {
    render(<DexterUi_TransactionIcon transactionType=ExchangeAndSend />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("AddLiquidity", () => {
    render(<DexterUi_TransactionIcon transactionType=AddLiquidity />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("RemoveLiquidity", () => {
    render(<DexterUi_TransactionIcon transactionType=RemoveLiquidity />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
