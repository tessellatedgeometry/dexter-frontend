open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("TZG; currenciesStateEmpty", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateEmpty,
        }>
        <DexterUi_CurrencyValue
          token=Fixtures_Balances.tzgExchangeBalance
          value={Token(Tezos.Token.mkToken(Bigint.of_int64(1000L), 0))}
        />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("TZG; currenciesStateUSD", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateUSD,
        }>
        <DexterUi_CurrencyValue
          token=Fixtures_Balances.tzgExchangeBalance
          value={Token(Tezos.Token.mkToken(Bigint.of_int64(1000L), 0))}
        />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("TZG; currenciesStateBTC", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateBTC,
        }>
        <DexterUi_CurrencyValue
          token=Fixtures_Balances.tzgExchangeBalance
          value={Token(Tezos.Token.mkToken(Bigint.of_int64(1000L), 0))}
        />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("TZG; currenciesStateError", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateError,
        }>
        <DexterUi_CurrencyValue
          token=Fixtures_Balances.tzgExchangeBalance
          value={Token(Tezos.Token.mkToken(Bigint.of_int64(1000L), 0))}
        />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("XTZ; currenciesStateUSD", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateUSD,
        }>
        <DexterUi_CurrencyValue value={Mutez(Mutez(10000000L))} />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("XTZ; currenciesStateBTC", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateBTC,
        }>
        <DexterUi_CurrencyValue value={Mutez(Mutez(10000000L))} />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
