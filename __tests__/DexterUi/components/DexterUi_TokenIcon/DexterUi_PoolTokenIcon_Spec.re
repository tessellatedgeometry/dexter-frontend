open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Normal XTZ/tzBTC", () => {
    render(
      <DexterUi_PoolTokenIcon
        icons=("/img/tezos.png", "/img/tzBTC-logo.png")
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Small tzBTZ/USDtz", () => {
    render(
      <DexterUi_PoolTokenIcon
        icons=("/img/tzBTC-logo.png", "/img/USDtz-logo.png")
        small=true
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
