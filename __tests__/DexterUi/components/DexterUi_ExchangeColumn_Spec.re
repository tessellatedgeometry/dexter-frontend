open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("MTZ; No tokenSearchBalances;", () => {
    render(
      <DexterUi_ExchangeColumn
        balanceInfo=["Snapshot balanceInfo 1", "Snapshot balanceInfo 2"]
        title={"Snapshot title" |> React.string}
        token=Fixtures_Balances.mtzBalance>
        {"Snapshot" |> React.string}
      </DexterUi_ExchangeColumn>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("XTZ/MTZ; No tokenSearchBalances; No title;", () => {
    render(
      <DexterUi_ExchangeColumn
        balanceInfo=["Snapshot balanceInfo 1", "Snapshot balanceInfo 2"]
        poolToken=(
          Fixtures_Balances.xtzTenTezBalance,
          Fixtures_Balances.mtzBalance,
        )>
        {"Snapshot" |> React.string}
      </DexterUi_ExchangeColumn>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("XTZ/TZG; With tokenSearchBalances;", () => {
    render(
      <DexterUi_ExchangeColumn
        balanceInfo=["Snapshot balanceInfo 1", "Snapshot balanceInfo 2"]
        title={"Snapshot title" |> React.string}
        poolToken=(
          Fixtures_Balances.xtzTenTezBalance,
          Fixtures_Balances.tzgBalance,
        )
        tokenSearchBalances=Fixtures_Balances.balances1>
        {"Snapshot" |> React.string}
      </DexterUi_ExchangeColumn>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
