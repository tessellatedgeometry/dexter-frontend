open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Display only;", () => {
    render(
      <DexterUi_ExchangeInput>
        {"Snapshot" |> React.string}
      </DexterUi_ExchangeInput>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Simple; No usd value; With MAX button;", () => {
    render(
      <DexterUi_ExchangeInput onClickMax={_ => ()}>
        {"Snapshot" |> React.string}
      </DexterUi_ExchangeInput>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Simple; With usd value; Disabled;", () => {
    render(
      <DexterUi_ExchangeInput isDisabled=true>
        {"Snapshot" |> React.string}
      </DexterUi_ExchangeInput>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Simple; With usd value; Invalid;", () => {
    render(
      <DexterUi_ExchangeInput isValid=false>
        {"Snapshot" |> React.string}
      </DexterUi_ExchangeInput>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
