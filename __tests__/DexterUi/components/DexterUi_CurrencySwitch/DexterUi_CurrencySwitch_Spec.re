open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("currenciesStateEmpty", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateEmpty,
        }>
        <DexterUi_CurrencySwitch />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currenciesStateUSD", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateUSD,
        }>
        <DexterUi_CurrencySwitch />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currenciesStateBTC", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateBTC,
        }>
        <DexterUi_CurrencySwitch />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currenciesStateError", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateError,
        }>
        <DexterUi_CurrencySwitch />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
