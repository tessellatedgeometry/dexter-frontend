open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("currenciesStateUSD; currencyUSD", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateUSD,
        }>
        <DexterUi_CurrencySwitchListOption
          currency=Fixtures_CurrenciesState.currencyUSD
        />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currenciesStateBTC; currencyUSD", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateBTC,
        }>
        <DexterUi_CurrencySwitchListOption
          currency=Fixtures_CurrenciesState.currencyUSD
        />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currenciesStateBTC; currencyBTC", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateBTC,
        }>
        <DexterUi_CurrencySwitchListOption
          currency=Fixtures_CurrenciesState.currencyBTC
        />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currenciesStateUSD; currencyJPY", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateUSD,
        }>
        <DexterUi_CurrencySwitchListOption
          currency=Fixtures_CurrenciesState.currencyJPY
        />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
