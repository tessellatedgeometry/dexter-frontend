open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("currenciesStateUSD", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateUSD,
        }>
        <DexterUi_CurrencySwitchList onClose={() => ()} />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currenciesStateBTC", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateBTC,
        }>
        <DexterUi_CurrencySwitchList onClose={() => ()} />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currenciesStateError", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateError,
        }>
        <DexterUi_CurrencySwitchList onClose={() => ()} />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
