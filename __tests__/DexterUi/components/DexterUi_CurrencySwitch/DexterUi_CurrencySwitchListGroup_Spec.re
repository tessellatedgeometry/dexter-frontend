open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("currenciesStateUSD; popular; empty value", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateUSD,
        }>
        <DexterUi_CurrencySwitchListGroup popular=true value="" />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currenciesStateBTC; popular; value=BTC", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateBTC,
        }>
        <DexterUi_CurrencySwitchListGroup popular=true value="BTC" />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currenciesStateUSD; popular; value=ABC", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateUSD,
        }>
        <DexterUi_CurrencySwitchListGroup popular=true value="ABC" />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currenciesStateUSD; not popular; value empty", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateUSD,
        }>
        <DexterUi_CurrencySwitchListGroup value="" />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currenciesStateBTC; not popular; value=Sri", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateBTC,
        }>
        <DexterUi_CurrencySwitchListGroup value="Sri" />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("currenciesStateBTC; not popular; value=ABC", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          currenciesState: Fixtures_CurrenciesState.currenciesStateBTC,
        }>
        <DexterUi_CurrencySwitchListGroup value="ABC" />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
