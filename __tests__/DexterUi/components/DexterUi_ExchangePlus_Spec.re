open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Normal", () => {
    render(<DexterUi_ExchangePlus />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Blue", () => {
    render(<DexterUi_ExchangePlus blue=true />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
