open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Light mode; Primary; Horizontal padding 10px", () => {
    render(
      <DexterUi_Context.ProviderInternal value=DexterUi_Context.defaultValue>
        <DexterUi_Button px={`px(10)}>
          {"Snapshot" |> React.string}
        </DexterUi_Button>
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Light mode; Secondary; Disabled; Horizontal padding 10px", () => {
    render(
      <DexterUi_Context.ProviderInternal value=DexterUi_Context.defaultValue>
        <DexterUi_Button px={`px(10)} variant=Secondary disabled=true>
          {"Snapshot" |> React.string}
        </DexterUi_Button>
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Dark mode; Primary; Small; Width 120px", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={...DexterUi_Context.defaultValue, darkMode: true}>
        <DexterUi_Button width={`px(120)} small=true>
          {"Snapshot" |> React.string}
        </DexterUi_Button>
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Dark mode; Primary; Dark mode disabled; Width 120px", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={...DexterUi_Context.defaultValue, darkMode: true}>
        <DexterUi_Button width={`px(120)} darkModeEnabled=false>
          {"Snapshot" |> React.string}
        </DexterUi_Button>
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
