open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Renders", () => {
    render(
      <DexterUi_Link href="#"> {"Snapshot" |> React.string} </DexterUi_Link>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  })
});
