open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("No account", () => {
    render(
      <DexterUi_Context.ProviderInternal value=DexterUi_Context.defaultValue>
        <DexterUi_LiquidityExplainerInfoRow
          boxHeight={`px(118)}
          number=1
          image="/img/liquidity-explainer-1.png">
          {"Snapshot" |> React.string}
        </DexterUi_LiquidityExplainerInfoRow>
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("With account", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          account: Some(Fixtures_Account.account1),
        }>
        <DexterUi_LiquidityExplainerInfoRow
          boxHeight={`px(118)}
          number=1
          image="/img/liquidity-explainer-1.png">
          {"Snapshot" |> React.string}
        </DexterUi_LiquidityExplainerInfoRow>
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
