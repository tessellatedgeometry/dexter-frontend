open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Non-empty balances", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          balances: Fixtures_Balances.balances1,
        }>
        <DexterUi_WalletInfoBalances />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Empty balances", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={...DexterUi_Context.defaultValue, balances: []}>
        <DexterUi_WalletInfoBalances />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
