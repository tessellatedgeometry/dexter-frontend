open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Exchange ; Dark mode; Failed", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          account: Some(Fixtures_Account.account1),
          transactions: Some([Fixtures_Transactions.exchange]),
          darkMode: true,
          transactionStatus: Failed,
        }>
        <DexterUi_Status />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Exchange ; Dark mode; Applied", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          account: Some(Fixtures_Account.account1),
          transactions: Some([Fixtures_Transactions.exchange]),
          darkMode: true,
          transactionStatus: Applied,
        }>
        <DexterUi_Status />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Exchange ; Light mode; Pending", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          account: Some(Fixtures_Account.account1),
          transactions: Some([Fixtures_Transactions.exchange]),
          transactionStatus: Pending,
        }>
        <DexterUi_Status />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Exchange and send; Dark mode; Applied", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          account: Some(Fixtures_Account.account1),
          transactions: Some([Fixtures_Transactions.exchangeAndSend]),
          darkMode: true,
          transactionStatus: Applied,
        }>
        <DexterUi_Status />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Add liquidity; Light mode; Applied", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          account: Some(Fixtures_Account.account1),
          transactions: Some([Fixtures_Transactions.addLiquidity]),
          transactionStatus: Applied,
        }>
        <DexterUi_Status />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Remove liquidity; Light mode; Failed", () => {
    render(
      <DexterUi_Context.ProviderInternal
        value={
          ...DexterUi_Context.defaultValue,
          account: Some(Fixtures_Account.account1),
          transactions: Some([Fixtures_Transactions.removeLiquidity]),
          transactionStatus: Failed,
        }>
        <DexterUi_Status />
      </DexterUi_Context.ProviderInternal>,
    )
    |> container
    |> expect
    |> toMatchSnapshot
  });
});
