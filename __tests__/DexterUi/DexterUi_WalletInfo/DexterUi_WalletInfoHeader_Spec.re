open Jest;
open Expect;
open ReactTestingLibrary;

let mockDappClient: unit => Beacon.dappClient = [%bs.raw
  {|
   function() {
     return "";
   }
  |}
];

describe("Snapshots", () => {
  test("TezBridge", () => {
    render(<DexterUi_WalletInfoHeader account=Fixtures_Account.account1 />)
    |> container
    |> expect
    |> toMatchSnapshot
  });
  test("Beacon", () => {
    let dappClient = mockDappClient();

    render(
      <DexterUi_WalletInfoHeader
        account={
          ...Fixtures_Account.account1,
          wallet: Tezos_Wallet.Beacon(dappClient),
        }
      />,
    )
    |> container
    |> expect
    |> toMatchSnapshot;
  });
});
