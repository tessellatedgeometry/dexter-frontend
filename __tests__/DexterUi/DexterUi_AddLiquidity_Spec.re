open Jest;
open Expect;

let xtz: Tezos.Mutez.t = Tezos.Mutez.Mutez(100000000L);
let xtzValue: Valid.t(Tezos.Mutez.t) = Valid(Tezos.Mutez.oneTez, "");
let tokenValue: Valid.t(Tezos.Token.t) = Valid(Tezos.Token.one, "");
let inputToken: Dexter_ExchangeBalance.t = {
  name: "",
  symbol: "",
  icon: "",
  tokenContract: Tezos.Contract.Contract(""),
  dexterContract: Tezos.Contract.Contract(""),
  dexterBaker: None,
  tokenBalance: Tezos.Token.one,
  lqtBalance: Tezos.Token.one,
  exchangeTotalLqt: Tezos.Token.one,
  exchangeXtz: Tezos.Mutez.oneTez,
  exchangeTokenBalance: Tezos.Token.one,
  exchangeAllowanceForAccount: Tezos.Token.zero,
};

describe("disableAddLiquidity", () => {
  test("values are less than limits", () => {
    expect(
      DexterUi_AddLiquidity_Utils.checkIsInvalid(
        xtz,
        xtzValue,
        tokenValue,
        Some(inputToken),
      ),
    )
    |> toEqual(false)
  });

  test("xtz is greater than limit", () => {
    let xtzValue: Valid.t(Tezos.Mutez.t) =
      Valid(Tezos.Mutez.Mutez(200000000L), "");

    expect(
      DexterUi_AddLiquidity_Utils.checkIsInvalid(
        xtz,
        xtzValue,
        tokenValue,
        Some(inputToken),
      ),
    )
    |> toEqual(true);
  });

  test("xtz is greater than limit", () => {
    let xtzValue: Valid.t(Tezos.Mutez.t) =
      Valid(Tezos.Mutez.Mutez(200000000L), "");

    expect(
      DexterUi_AddLiquidity_Utils.checkIsInvalid(
        xtz,
        xtzValue,
        tokenValue,
        Some(inputToken),
      ),
    )
    |> toEqual(true);
  });

  test("xtzValue is invalid", () => {
    let xtzValue: Valid.t(Tezos.Mutez.t) = Invalid("10.");

    expect(
      DexterUi_AddLiquidity_Utils.checkIsInvalid(
        xtz,
        xtzValue,
        tokenValue,
        Some(inputToken),
      ),
    )
    |> toEqual(true);
  });

  test("token is greater than limit", () => {
    let tokenValue: Valid.t(Tezos.Token.t) =
      Valid(Tezos.Token.mkToken(Bigint.of_int64(20000000000L), 0), "");

    expect(
      DexterUi_AddLiquidity_Utils.checkIsInvalid(
        xtz,
        xtzValue,
        tokenValue,
        Some(inputToken),
      ),
    )
    |> toEqual(true);
  });

  test("tokenValue is invalid", () => {
    let tokenValue: Valid.t(Tezos.Token.t) = Invalid("10.");

    expect(
      DexterUi_AddLiquidity_Utils.checkIsInvalid(
        xtz,
        xtzValue,
        tokenValue,
        Some(inputToken),
      ),
    )
    |> toEqual(true);
  });

  test("inputToken is undefined", () => {
    let tokenValue: Valid.t(Tezos.Token.t) =
      Valid(Tezos.Token.mkToken(Bigint.of_int64(20000000000L), 0), "");

    expect(
      DexterUi_AddLiquidity_Utils.checkIsInvalid(
        xtz,
        xtzValue,
        tokenValue,
        None,
      ),
    )
    |> toEqual(true);
  });
});
