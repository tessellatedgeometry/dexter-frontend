type t = {
  decimals: int,
  name: string,
  prefixed: bool,
  rate: float,
  sign: option(string),
  symbol: string,
};

let default = "USD";

let popular: array(string) = [|
  "USD",
  "EUR",
  "GBP",
  "JPY",
  "RUB",
  "BTC",
  "ETH",
|];

let toStringWithCommas = (currency: t, tezFloat: float): string => {
  let symbol = currency.sign |> Utils.orDefault(" " ++ currency.symbol);

  (currency.prefixed ? symbol : "")
  ++ Common.addCommas(
       Js.Float.toFixedWithPrecision(
         tezFloat *. currency.rate,
         ~digits=currency.decimals,
       ),
     )
  ++ (!currency.prefixed ? symbol : "");
};

let findBySymbolOrDefault = (symbol: string, currencies: list(t)): option(t) =>
  switch (
    currencies |> List.find_opt((currency: t) => currency.symbol === symbol)
  ) {
  | Some(current) => Some(current)
  | _ =>
    currencies |> List.find_opt((currency: t) => currency.symbol === default)
  };
