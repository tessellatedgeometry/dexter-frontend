open Css;

type textRules = list(Css.rule);

let version: textRules = [
  lineHeight(px(22)),
  fontSize(px(16)),
  letterSpacing(px(2)),
  fontWeight(`semiBold),
  color(hex("242D3C")),
];
let default: textRules = [lineHeight(px(13)), fontSize(px(10))];
let small: textRules = [lineHeight(px(11)), fontSize(px(8))];
let bold: textRules = [fontWeight(`semiBold), ...default];
let h1: textRules = [
  lineHeight(px(25)),
  fontSize(px(18)),
  fontWeight(`semiBold),
];
let h2: textRules = [lineHeight(px(15)), fontSize(px(11))];
let h3: textRules = [
  fontWeight(`semiBold),
  letterSpacing(pxFloat(1.75)),
  textTransform(`uppercase),
  ...default,
];
let h4: textRules = [lineHeight(px(17)), fontSize(px(12))];
let mobileText: textRules = [lineHeight(px(22)), fontSize(px(16))];
