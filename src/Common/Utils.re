let renderOpt: ('a => React.element, option('a)) => React.element =
  (render, item) => {
    switch (item) {
    | Some(item) => render(item)
    | None => React.null
    };
  };

let flatMap = (map, data) => Belt.Option.flatMap(data, map);

let map = (map, data) => Belt.Option.map(data, map);

let orDefault = (default, value) =>
  switch (value) {
  | Some(value) => value
  | _ => default
  };

let renderIf = (element: React.element, condition: bool) =>
  condition ? element : React.null;

let filterNone = data => Belt.Array.keepMap(data, x => x);

let getDecimalsCountInString = (str: string) => {
  let splitted = str |> Js.String.split(".");
  splitted |> Array.length > 1 ? splitted[1] |> String.length : 0;
};

let omitIpfsInPath = (path: list(string)): list(string) => {
  switch (path |> Belt.List.head) {
  | Some("ipfs")
  | Some("ipns") =>
    path |> Array.of_list |> Js.Array.sliceFrom(2) |> Array.to_list
  | _ => path
  };
};

let useUrl = (): (list(string), string) => {
  let url = ReasonReactRouter.useUrl();
  (omitIpfsInPath(url.path), url.hash);
};

let getIpfsBaseUrlFromPath = (path: list(string)): string => {
  switch (path |> Belt.List.head) {
  | Some(head) when head === "ipfs" || head == "ipns" =>
    switch (path |> Belt.List.tail) {
    | Some(tail) => "/" ++ head ++ "/" ++ (tail |> List.hd) ++ "/"
    | _ => "/"
    }
  | _ => "/"
  };
};

let useBaseUrl = (): string => {
  let url = ReasonReactRouter.useUrl();
  getIpfsBaseUrlFromPath(url.path);
};
