open Css;

let black = hex("000000");
let blackish = hex("333333");
let blackish1 = hex("242D3C");
let blackish2 = hex("2c3249");
let blackish3 = hex("37405B");
let blackish4 = hex("47526E");
let blue = hex("2B3B9B");
let blueGrey = hex("EDEDFB");
let boxBackgroundDark = hex("222e42");
let darkBlue = hex("061cc1");
let darkBlueGrey = hex("DBDBF2");
let darkGrey1 = hex("758DA6");
let darkGrey2 = hex("62626E");
let darkRed = hex("A63C3C");
let green = hex("0CCA4A");
let grey = hex("82828B");
let grey1 = hex("474747");
let lightGrey = hex("979797");
let lightGrey1 = hex("E7E7F8");
let lightGrey2 = hex("F9F9F9");
let lightGrey3 = hex("E6E6E6");
let lightRed = hex("FFF6F6");
let lightRed2 = hex("F8E4E4");
let offBlack = hex("364359");
let offWhite = hex("F6F6FC");
let primary = hex("26325C");
let primaryDark = hex("8996C4");
let red = hex("E25453");
let secondaryTabActiveDark = hex("1D2532");
let tabInactiveDark = hex("4C5D79");
let violet = hex("7585FF");
let white = hex("FFFFFF");
let whiteGray = hex("FAFAFF");

let text = darkMode => darkMode ? offWhite : grey;
let line = darkMode => darkMode ? blackish4 : lightGrey3;
let boxBackground = darkMode => darkMode ? boxBackgroundDark : white;
