/*
 { "block": $block_header,
     "save_point": integer ∈ [-2^31-2, 2^31+2],
     "caboose": integer ∈ [-2^31-2, 2^31+2],
     "history_mode": "full" | "archive" | "rolling" }
  */
type t = {
  block: Tezos_BlockHeader.t,
  savePoint: int,
  caboose: int,
  historyMode: string,
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  Json.Decode.(
    switch (
      field(
        "block",
        a => Tezos_Util.unwrapResult(Tezos_BlockHeader.decode(a)),
        json,
      ),
      field("save_point", int, json),
      field("caboose", int, json),
      field("history_mode", string, json),
    ) {
    | (block, savePoint, caboose, historyMode) =>
      Belt.Result.Ok({block, savePoint, caboose, historyMode})
    | exception (DecodeError(error)) => Belt.Result.Error(error)
    }
  );
};
