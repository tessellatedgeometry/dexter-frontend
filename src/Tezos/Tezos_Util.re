let explode = s => List.init(String.length(s), String.get(s));

let isDigit = char => {
  let c = Char.code(char);
  c >= 48 && c <= 57;
};

let isStringOfDigits = s =>
  if (String.length(s) === 0) {
    false;
  } else {
    List.fold_right((c, x) => isDigit(c) && x, explode(s), true);
  };

let bigintOfString = (t: string): option(Bigint.t) =>
  if (isStringOfDigits(t)) {
    Some(Bigint.of_string(t));
  } else {
    None;
  };

let bigintEncode = t => Json.Encode.string(Bigint.to_string(t));
let bigintDecode = json =>
  switch (Json.Decode.string(json)) {
  | v =>
    switch (bigintOfString(v)) {
    | Some(v) => Belt.Result.Ok(v)
    | None => Belt.Result.Error("bigintDecode failed: " ++ v)
    }
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("bigintDecode failed: " ++ error)
  };

let unwrapResult = r =>
  switch (r) {
  | Belt.Result.Ok(v) => v
  | Belt.Result.Error(message) => raise @@ Json.Decode.DecodeError(message)
  };

let floatOfString = (x: string): option(float) =>
  try(Some(float_of_string(x))) {
  | Failure(_) => None
  };

let int64OfString = (x: string): option(Int64.t) =>
  try(Some(Int64.of_string(x))) {
  | Failure(_) => None
  };

let explode = s => {
  let rec exp = (i, l) =>
    if (i < 0) {
      l;
    } else {
      exp(i - 1, [s.[i], ...l]);
    };
  exp(String.length(s) - 1, []);
};

let getDecimalsCount = (floatString: string): int => {
  let split = Js.String.split(".", floatString);
  Array.length(split) == 2 ? String.length(split[1]) : 0;
};

let removeRedundantDecimals = (floatString: string, decimals: int): string => {
  let split = Js.String.split(".", floatString);
  if (Array.length(split) == 2 && String.length(split[1]) > decimals) {
    split[0] ++ "." ++ Js.String.slice(~from=0, ~to_=decimals, split[1]);
  } else {
    floatString;
  };
};

let getPositionOfSmallestNonZero = (float: float): int => {
  let floatString = Js.Float.toString(float);
  let split = Js.String.split(".", floatString);
  if (Array.length(split) == 1) {
    0;
  } else if (Array.length(split) == 2) {
    let str = split[1];
    let chars = explode(str);
    let length = List.length(chars);
    let charsRev = List.rev(chars);

    let (i, _b) =
      List.fold_left(
        ((l, finished), char) =>
          if (finished) {
            (l, finished);
          } else if (char == '0') {
            (l - 1, false);
          } else {
            (l, true);
          },
        (length, false),
        charsRev,
      );
    i;
  } else {
    0;
  };
};

let rec repeatString = (n, s) =>
  if (n == 0) {
    "";
  } else {
    s ++ repeatString(n - 1, s);
  };

let rec pow = a =>
  fun
  | 0 => 1
  | 1 => a
  | n => {
      let b = pow(a, n / 2);
      b
      * b
      * (
        if (n mod 2 == 0) {
          1;
        } else {
          a;
        }
      );
    };

let decodeSafeList =
    (decode: Js.Json.t => Belt.Result.t('a, string), json: Js.Json.t)
    : list('a) =>
  if (Js.Array.isArray(json)) {
    let source: array(Js.Json.t) = Obj.magic(json: Js.Json.t);
    let length = Js.Array.length(source);
    let output = ref([]);
    for (i in 0 to length - 1) {
      let value = decode(Array.unsafe_get(source, i));
      switch (value) {
      | Belt.Result.Ok(value) => output := List.append(output^, [value])
      | Belt.Result.Error(_) => ()
      };
    };
    output^;
  } else {
    raise @@
    Json.Decode.DecodeError(
      "Expected array, got " ++ Js.Json.stringify(json),
    );
  };
