type t =
  | ImplicitAccount(Tezos_ImplicitAccount.t)
  | Contract(Tezos_Contract.t);

let encode = (t: t): Js.Json.t =>
  switch (t) {
  | ImplicitAccount(t) => Tezos_ImplicitAccount.encode(t)
  | Contract(t) => Tezos_Contract.encode(t)
  };

let decode = (json: Js.Json.t): Belt.Result.t(t, string) =>
  switch (Tezos_Contract.decode(json)) {
  | Belt.Result.Ok(c) => Belt.Result.Ok(Contract(c))
  | Belt.Result.Error(_) =>
    switch (Tezos_ImplicitAccount.decode(json)) {
    | Belt.Result.Ok(c) => Belt.Result.Ok(ImplicitAccount(c))
    | Belt.Result.Error(_) =>
      Belt.Result.Error("Address.decode failed: string is not an address.")
    }
  };

let ofContract = (c: Tezos_Contract.t) => Contract(c);

let ofImplicitAccount = (c: Tezos_ImplicitAccount.t) => ImplicitAccount(c);

let zero = ImplicitAccount(Tezos_ImplicitAccount.zero);

let ofString = (candidate: string): Belt.Result.t(t, string) =>
  switch (Tezos_Contract.ofString(candidate)) {
  | Belt.Result.Ok(c) => Belt.Result.Ok(Contract(c))
  | _ =>
    switch (Tezos_ImplicitAccount.ofString(candidate)) {
    | Belt.Result.Ok(c) => Belt.Result.Ok(ImplicitAccount(c))
    | _ =>
      Belt.Result.Error(
        "Address.ofString: unexpected candidate string: " ++ candidate,
      )
    }
  };

let toString = (t: t): string =>
  switch (t) {
  | ImplicitAccount(t) => Tezos_ImplicitAccount.toString(t)
  | Contract(t) => Tezos_Contract.toString(t)
  };

let pack = (t: t): string =>
  switch (t) {
  | ImplicitAccount(t) => Tezos_ImplicitAccount.pack(t)
  | Contract(t) => Tezos_Contract.pack(t)
  };

let toScriptExpr = (t: t): string =>
  switch (t) {
  | ImplicitAccount(t) => Tezos_ImplicitAccount.toScriptExpr(t)
  | Contract(t) => Tezos_Contract.toScriptExpr(t)
  };

let isEqual = (t0: t, t1: t) =>
  switch (t0, t1) {
  | (ImplicitAccount(_), ImplicitAccount(_))
  | (Contract(_), Contract(_)) => t0 |> toString === (t1 |> toString)
  | _ => false
  };

type s = t;

module Comparable =
  Belt.Id.MakeComparable({
    type t = s;
    let cmp = (c0, c1) =>
      switch (c0, c1) {
      | (ImplicitAccount(str0), ImplicitAccount(str1)) =>
        Pervasives.compare(str0, str1)
      | (Contract(str0), Contract(str1)) => Pervasives.compare(str0, str1)
      | _ => (-1)
      };
  });
