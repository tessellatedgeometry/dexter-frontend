/*
 { "level": integer ∈ [-2^31-2, 2^31+2],
       "proto": integer ∈ [0, 255],
       "predecessor": $block_hash,
       "timestamp": $timestamp.protocol,
       "validation_pass": integer ∈ [0, 255],
       "operations_hash": $Operation_list_list_hash,
       "fitness": $fitness,
       "context": $Context_hash,
       "priority": integer ∈ [0, 2^16-1],
       "proof_of_work_nonce": /^[a-zA-Z0-9]+$/,
       "seed_nonce_hash"?: $cycle_nonce,
       "signature": $Signature }

  */

type t = {
  level: int,
  predecessor: string,
  timestamp: Tezos_Timestamp.t,
};

let encode = (t: t): Js.Json.t => {
  Json.Encode.(
    object_([
      ("level", int(t.level)),
      ("predecessor", string(t.predecessor)),
      ("timestamp", Tezos_Timestamp.encode(t.timestamp)),
    ])
  );
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  Json.Decode.(
    switch (
      field("level", int, json),
      field("predecessor", string, json),
      field(
        "timestamp",
        a => Tezos_Util.unwrapResult(Tezos_Timestamp.decode(a)),
        json,
      ),
    ) {
    | (level, predecessor, timestamp) =>
      Belt.Result.Ok({level, predecessor, timestamp})
    | exception (DecodeError(error)) => Belt.Result.Error(error)
    }
  );
};
