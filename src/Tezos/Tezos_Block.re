// $operation
module Operation = {
  module Content = {
    type t =
      | ContentsAndResults(Tezos_Operation_Alpha.Contents.Result.t)
      | Contents(Tezos_Operation_Alpha.Contents.t);

    let decode = (json: Js.Json.t) => {
      switch (Tezos_Operation_Alpha.Contents.Result.decode(json)) {
      | Belt.Result.Ok(o) => Belt.Result.Ok(ContentsAndResults(o))
      | Belt.Result.Error(_error) =>
        switch (Tezos_Operation_Alpha.Contents.decode(json)) {
        | Belt.Result.Ok(o) => Belt.Result.Ok(Contents(o))
        | Belt.Result.Error(_error) =>
          Belt.Result.Error("Unable to decode operation")
        }
      };
    };
  };

  type t = {
    chainId: string,
    hash: string,
    branch: string,
    contents: list(Content.t),
  };

  let decode = (json: Js.Json.t) => {
    switch (
      Json.Decode.(
        field("chain_id", string, json),
        field("hash", string, json),
        field("branch", string, json),
        field(
          "contents",
          list(a => Tezos_Util.unwrapResult(Content.decode(a))),
          json,
        ),
      )
    ) {
    | (chainId, hash, branch, contents) =>
      Belt.Result.Ok({chainId, hash, branch, contents})
    | exception (Json.Decode.DecodeError(error)) => Belt.Result.Error(error)
    };
  };
};

type t = {
  chainId: string,
  hash: string,
  header: Tezos_RawBlockHeader.t,
  operations: list(list(Operation.t)),
};

let encode = (t: t): Js.Json.t => {
  Json.Encode.(
    object_([
      ("chain_id", string(t.chainId)),
      ("operationId", string(t.chainId)),
      ("header", Tezos_RawBlockHeader.encode(t.header)),
    ])
  );
};

let decode = (json: Js.Json.t): Belt.Result.t(t, string) => {
  Json.Decode.(
    switch (
      field("chain_id", string, json),
      field("hash", string, json),
      field(
        "header",
        a => Tezos_Util.unwrapResult(Tezos_RawBlockHeader.decode(a)),
        json,
      ),
      field(
        "operations",
        a =>
          list(
            b => list(c => Tezos_Util.unwrapResult(Operation.decode(c)), b),
            a,
          ),
        json,
      ),
    ) {
    | (chainId, hash, header, operations) =>
      Belt.Result.Ok({chainId, hash, header, operations})
    | exception (DecodeError(error)) => Belt.Result.Error(error)
    }
  );
};

type transaction =
  | TransactionWithResult(Tezos_Operation_Alpha.Contents.Result.Transaction.t)
  | TransactionWithoutResult(Tezos_Operation_Alpha.Contents.Transaction.t);

let catSomes = (type a, mas: list(option(a))): list(a) =>
  List.fold_left(
    (acc, x) =>
      switch (x) {
      | None => acc
      | Some(a) => List.append(acc, [a])
      },
    [],
    mas,
  );

let getTransactions = (block: t): list(transaction) => {
  List.map(
    (operation: Operation.Content.t) => {
      switch (operation) {
      | ContentsAndResults(o) =>
        switch (o) {
        | Transaction(t) => Some(TransactionWithResult(t))
        | _ => None
        }
      | Contents(o) =>
        switch (o) {
        | Transaction(t) => Some(TransactionWithoutResult(t))
        | _ => None
        }
      }
    },
    List.concat(
      List.map(
        (operation: Operation.t) => operation.contents,
        List.concat(block.operations),
      ),
    ),
  )
  |> catSomes;
};

let getTransactionsByOperationId =
    (operationId: string, block: t): list(transaction) => {
  List.map(
    (operation: Operation.Content.t) => {
      switch (operation) {
      | ContentsAndResults(o) =>
        switch (o) {
        | Transaction(t) => Some(TransactionWithResult(t))
        | _ => None
        }
      | Contents(o) =>
        switch (o) {
        | Transaction(t) => Some(TransactionWithoutResult(t))
        | _ => None
        }
      }
    },
    List.concat(
      List.map(
        (operation: Operation.t) => operation.contents,
        List.filter(
          (operation: Operation.t) => operation.hash == operationId,
          List.concat(block.operations),
        ),
      ),
    ),
  )
  |> catSomes;
};

// get transactions that were sent to a specific contract in a block
let getContractTransactions =
    (contractId: string, operationId: string, block: t) => {
  List.filter(
    (transaction: transaction) => {
      switch (transaction) {
      | TransactionWithResult(t) => t.destination == contractId
      | TransactionWithoutResult(t) => t.destination == contractId
      }
    },
    getTransactionsByOperationId(operationId, block),
  );
};

// get the result from transaction if it exists
let getTransactionResult =
    (transaction: transaction)
    : option(Tezos_Operation_Alpha.Contents.Result.Transaction.Metadata.t) => {
  switch (transaction) {
  | TransactionWithResult(t) => Some(t.metadata)
  | TransactionWithoutResult(_) => None
  };
};
