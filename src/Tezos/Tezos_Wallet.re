type t =
  | Beacon(Beacon.dappClient)
  | TezBridge;

module Operation = {
  module Response = {
    type t =
      | Beacon(Beacon.Tezos.Operation.Response.t)
      | TezBridge(TezBridge.Transaction.Response.t);

    let getTransactionHash = (t: t): string =>
      switch (t) {
      | Beacon(r) => r.transactionHash
      | TezBridge(r) => r.operationId
      };
  };
};
