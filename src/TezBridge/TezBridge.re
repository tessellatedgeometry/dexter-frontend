/**
 * get address from Tezbridge
 */
let getSourceRaw: unit => Js.Promise.t(string) = [%bs.raw
  {|
    function () {
        return tezbridge.request({method: 'get_source'});
    }
    |}
];

let getSource = (): Js.Promise.t(Tezos.Address.t) => {
  getSourceRaw()
  |> Js.Promise.then_(addressString => {
       Common.unwrapResult(Tezos.Address.ofString(addressString))
       |> Js.Promise.resolve
     });
};

/**
 * {operation_id: "oo42d1LB76JFUFMDMznhmuSQnneNLJkMqtSG8GRD2mwdMLvHumU", originated_contracts: Array(0)}
 */
module Transaction = {
  module Response = {
    module Raw = {
      type t = {
        [@bs.as "operation_id"]
        operationId: string,
        [@bs.as "originated_contracts"]
        originatedContracts: array(string),
      };
    };

    type t = {
      operationId: string,
      originatedContracts: list(string),
    };

    let fromRaw = (raw: Raw.t): t => {
      operationId: raw.operationId,
      originatedContracts: raw.originatedContracts |> Array.to_list,
    };
  };
};

/**
 * only used here, unsafe types
 */
let postTransactionRaw:
  array(Tezos_Operation.Raw.t) => Js.Promise.t(Transaction.Response.Raw.t) = [%bs.raw
  {|
   function (inject_operations) {
       return tezbridge.request({
         method: 'inject_operations',
         operations: inject_operations
       });
   }
   |}
];

let postTransactions =
    (transactions: list(Tezos_Operation.t))
    : Js.Promise.t(Transaction.Response.t) =>
  postTransactionRaw(
    transactions |> List.map(Tezos_Operation.toRaw) |> Array.of_list,
  )
  |> Js.Promise.then_(result =>
       Transaction.Response.fromRaw(result) |> Js.Promise.resolve
     );
