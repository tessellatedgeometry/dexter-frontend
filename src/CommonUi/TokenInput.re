[@react.component]
let make =
    (
      ~currencyValue: option(React.element)=?,
      ~disabled=false,
      ~isValid: bool,
      ~onChange: string => unit,
      ~onClickMax: option(unit => unit)=?,
      ~value: string,
    ) => {
  let {account, balances, darkMode, setTransactionStatus}: DexterUi_Context.t =
    DexterUi_Context.useContext();
  let (isFocused, setIsFocused) = React.useState(_ => false);

  let disabled =
    account
    |> Belt.Option.isNone
    || balances
    |> Dexter_Balance.isEmpty
    || disabled;

  let value =
    switch (isFocused, value |> Js.Float.fromString |> Js.Float.isNaN) {
    | (false, false) => Common.addCommas(value === "" ? "0" : value)
    | _ => value
    };

  let (localValue, setLocalValue) = React.useState(() => value);

  React.useEffect1(
    () => {
      if (!isValid && value === localValue && !disabled) {
        setTransactionStatus(Invalidated);
      };
      None;
    },
    [|isValid|],
  );

  React.useEffect1(
    () => {
      setLocalValue(_ => value);
      None;
    },
    [|value|],
  );

  <DexterUi_ExchangeInput
    isDisabled=disabled
    note=?currencyValue
    ?onClickMax
    isValid={isValid || disabled}>
    <input
      className=Css.(
        merge([
          style([
            flexGrow(1.),
            maxWidth(pct(100.)),
            color(darkMode ? Colors.white : Colors.black),
            selector("input:focus", [border(px(0), `none, Colors.white)]),
            border(px(0), `none, Colors.white),
            padding(`zero),
            background(`none),
            ...TextStyles.h1,
          ]),
        ])
      )
      disabled
      onBlur={_ => {
        let floatValue = value |> Js.Float.fromString;

        switch (floatValue |> Js.Float.isNaN) {
        | false => onChange(floatValue |> Js.Float.toString)
        | _ => ()
        };

        setIsFocused(_ => false);
      }}
      onFocus={_ => {
        if (value === "0") {
          onChange("");
        };
        setIsFocused(_ => true);
      }}
      type_="text"
      onChange={ev => onChange(ev |> Common.eventToValue)}
      value
    />
  </DexterUi_ExchangeInput>;
};
