/**
 * When the user provides an input value (what they sell to Dexter),
 * this calculates the output value (what they receive/buy from Dexter).
 */

let calculateInput =
    (
      inputToken: Dexter.Balance.t,
      outputToken: Dexter.Balance.t,
      outputValue: Valid.t(InputType.t),
    )
    : Valid.t(InputType.t) => {
  switch (inputToken, outputToken, outputValue) {
  // input is XTZ, output is token
  | (_, ExchangeBalance(outputToken), Valid(Token(outputValue), _)) =>
    let {exchangeXtz, exchangeTokenBalance}: Dexter.ExchangeBalance.t = outputToken;

    switch (
      Dexter.Exchange.tokenOutToXtzIn(
        outputValue,
        exchangeXtz,
        exchangeTokenBalance,
      )
    ) {
    | Some(inputValue) =>
      Dexter_Value.getMutezInputValue(
        ~invalid=Tezos.Mutez.gt(inputValue, exchangeXtz),
        inputValue,
      )
    | _ => InvalidInitialState
    };
  | (ExchangeBalance(inputToken), _, Valid(Mutez(xtzOut), _)) =>
    // input is token, output is XTZ
    let {exchangeXtz, exchangeTokenBalance}: Dexter.ExchangeBalance.t = inputToken;
    switch (
      Dexter.Exchange.xtzOutToTokenIn(
        xtzOut,
        exchangeXtz,
        exchangeTokenBalance,
      )
    ) {
    | Some(inputValue) =>
      Dexter_Value.getTokenInputValue(
        ~invalid=Tezos.Token.gt(inputValue, exchangeTokenBalance),
        inputValue,
      )
    | _ => InvalidInitialState
    };
  | _ => InvalidInitialState
  };
};

let calculateOutput =
    (
      inputToken: Dexter.Balance.t,
      inputValue: Valid.t(InputType.t),
      outputToken: Dexter.Balance.t,
    )
    : Valid.t(InputType.t) => {
  switch (inputToken, inputValue, outputToken) {
  // input is XTZ, output is token
  | (_, Valid(Mutez(inputValue), _), ExchangeBalance(outputToken)) =>
    let {exchangeXtz, exchangeTokenBalance}: Dexter.ExchangeBalance.t = outputToken;

    switch (
      Dexter.Exchange.xtzToToken(
        inputValue,
        exchangeXtz,
        exchangeTokenBalance,
      )
    ) {
    | Some(outputValue) =>
      Dexter_Value.getTokenInputValue(
        ~invalid=Tezos.Token.gt(outputValue, exchangeTokenBalance),
        outputValue,
      )
    | _ => InvalidInitialState
    };
  // input is token, output is XTZ
  | (ExchangeBalance(inputToken), Valid(Token(inputValue), _), _) =>
    let {exchangeXtz, exchangeTokenBalance}: Dexter.ExchangeBalance.t = inputToken;

    switch (
      Dexter.Exchange.tokenToXtz(
        inputValue,
        exchangeXtz,
        exchangeTokenBalance,
      )
    ) {
    | Some(outputValue) =>
      Dexter_Value.getMutezInputValue(
        ~invalid=Tezos.Mutez.gt(outputValue, exchangeXtz),
        outputValue,
      )
    | _ => InvalidInitialState
    };
  | _ => InvalidInitialState
  };
};
