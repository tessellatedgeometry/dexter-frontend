let getCustomSlippageChoice = (slippageString: string): Dexter_Slippage.t => {
  let decimalsCount = slippageString |> Utils.getDecimalsCountInString;

  Custom(
    switch (Tezos_Util.floatOfString(slippageString)) {
    | Some(slippage) =>
      slippage > 0.1 && slippage < 50.0 && decimalsCount < 3
        ? Valid(slippage, slippageString) : Invalid(slippageString)
    | None => Invalid(slippageString)
    },
  );
};

[@react.component]
let make =
    (
      ~setSlippageChoice: (Dexter_Slippage.t => Dexter_Slippage.t) => unit,
      ~slippageChoice: Dexter_Slippage.t,
    ) => {
  let (customSlippageChoice, setCustomSlippageChoice) =
    React.useState(_ => {
      Dexter_Slippage.Custom(
        switch (Dexter_LocalStorage.MaximumSlippage.get() |> Dexter_Slippage.ofFloat) {
        | Custom(v) => v
        | _ => InvalidInitialState
        },
      )
    });

  let (isSelected, isInvalid) =
    switch (slippageChoice) {
    | Custom(Valid(_)) => (true, false)
    | Custom(_) => (true, true)
    | _ => (false, false)
    };

  <DexterUi_SecondaryInput
    onChange={ev => {
      let customSlippageChoice =
        getCustomSlippageChoice(ev |> Common.eventToValue);
      setCustomSlippageChoice(_ => customSlippageChoice);
      setSlippageChoice(_ => customSlippageChoice);
    }}
    onFocus={_ => setSlippageChoice(_ => customSlippageChoice)}
    placeholder="5.55"
    translucent={!isSelected}
    noDarkMode=true
    inputBackground=Colors.white
    inputColor=?{isInvalid ? Some(Colors.red) : None}
    inputBorderColor=?{
      isInvalid ? Some(Colors.red) : isSelected ? Some(Colors.blue) : None
    }
    inputTextAlign=`right
    icon={
      <Flex mr={`px(-3)}>
        <Text color={isInvalid ? Colors.red : Colors.grey}>
          {"%" |> React.string}
        </Text>
      </Flex>
    }
    pl={`px(5)}
    pr={`px(13)}
    value={
      switch (slippageChoice) {
      | Custom(Valid(_, s))
      | Custom(Invalid(s)) => s
      | Custom(InvalidInitialState) => ""
      | _ => ""
      }
    }
    width={`px(45)}
  />;
};
