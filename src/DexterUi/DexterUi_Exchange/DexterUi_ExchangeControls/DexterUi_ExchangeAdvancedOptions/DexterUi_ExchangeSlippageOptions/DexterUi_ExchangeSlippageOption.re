[@react.component]
let make =
    (
      ~option: Dexter_Slippage.t,
      ~slippageChoice: Dexter_Slippage.t,
      ~setSlippageChoice: (Dexter_Slippage.t => Dexter_Slippage.t) => unit,
    ) => {
  let isSelected = option === slippageChoice;
  let optionText =
    switch (option) {
    | PointOne => "0.1%"
    | Half => "0.5% (default)"
    | One => "1%"
    | _ => ""
    };

  <Flex
    onClick={_ => setSlippageChoice(_ => option)}
    background={isSelected ? Colors.white : Colors.lightGrey1}
    height={`px(22)}
    alignItems=`center
    px={`px(8)}
    mr={`px(8)}
    borderRadius={`px(11)}>
    <Text
      className=Css.(
        style([
          before([
            display(`block),
            contentRule(`text(optionText)),
            fontWeight(`bold),
            height(`zero),
            overflow(`hidden),
            visibility(`hidden),
          ]),
        ])
      )
      color={isSelected ? Colors.blue : Colors.grey}
      fontWeight={isSelected ? `semiBold : `normal}
      whiteSpace=`nowrap>
      {optionText |> React.string}
    </Text>
  </Flex>;
};
