open DexterUi_Exchange_Utils;

[@react.component]
let make =
    (
      ~isExchangeDisabled: bool,
      ~resetInputs: unit => unit,
      ~sendTo: bool,
      ~sendToAddress: string,
      ~slippageRate: option(float),
      ~state: DexterUi_Exchange_Reducer.state,
    ) => {
  let {account, pushTransaction, transactionTimeout}: DexterUi_Context.t =
    DexterUi_Context.useContext();

  <DexterUi_Button
    px={`px(20)}
    disabled=isExchangeDisabled
    onClick={_ =>
      switch (account) {
      | Some(account) when !isExchangeDisabled =>
        onExchange(
          account,
          pushTransaction,
          resetInputs,
          sendTo,
          sendToAddress,
          slippageRate,
          state,
          transactionTimeout,
        )
      | _ => ()
      }
    }>
    {sendTo
       ? "Exchange "
         ++ Dexter.Balance.getSymbol(state.inputToken)
         ++ " and send "
         ++ Dexter.Balance.getSymbol(state.outputToken)
         |> React.string
       : "Exchange "
         ++ Dexter.Balance.getSymbol(state.inputToken)
         ++ " for "
         ++ Dexter.Balance.getSymbol(state.outputToken)
         |> React.string}
  </DexterUi_Button>;
};
