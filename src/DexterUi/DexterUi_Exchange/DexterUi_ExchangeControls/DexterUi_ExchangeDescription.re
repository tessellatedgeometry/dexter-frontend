[@react.component]
let make =
    (
      ~isExchangeDisabled: bool,
      ~expectedSlippageRate: option(float),
      ~slippageChoice: Dexter_Slippage.t,
      ~state: DexterUi_Exchange_Reducer.state,
    ) => {
  let {account, balances, transactionStatus}: DexterUi_Context.t =
    DexterUi_Context.useContext();

  <Flex my={`px(12)} height={`px(30)} alignItems=`flexEnd>
    <Text
      lightModeColor=Colors.grey1 textStyle=TextStyles.h2 textAlign=`center>
      {switch (account, balances |> Dexter_Balance.isEmpty, transactionStatus) {
       | (None, _, _) =>
         "Please connect a wallet to use the Dexter Exchange." |> React.string
       | (_, true, _) =>
         "Add some Tezos tokens to your connected wallet to use the Dexter Exchange."
         |> React.string
       | (_, _, Pending) => Common.pendingTransactionMessage
       | _ when !isExchangeDisabled =>
         "You are exchanging "
         ++ Dexter_Value.inputValueToStringWithCommas(state.inputValue)
         ++ " "
         ++ Dexter_Balance.getSymbol(state.inputToken)
         ++ " for a minimum of "
         ++ Dexter_Value.inputValueMinimum(
              state.outputValue,
              expectedSlippageRate,
            )
         ++ " "
         ++ Dexter_Balance.getSymbol(state.outputToken)
         ++ " to a maximum of "
         ++ Dexter_Value.inputValueToStringWithCommas(state.outputValue)
         ++ " "
         ++ Dexter_Balance.getSymbol(state.outputToken)
         ++ " with a price slippage limit of "
         ++ (slippageChoice |> Dexter_Slippage.toString)
         ++ ". A liquidity fee of 0.3% ("
         ++ Dexter_Value.inputValueFee(state.outputValue)
         ++ " "
         ++ Dexter.Balance.getSymbol(state.outputToken)
         ++ ") has been applied to this exchange."
         |> React.string
       | _ => Common.nbsp
       }}
    </Text>
  </Flex>;
};
