open Utils;

[@react.component]
let make =
    (
      ~children: React.element,
      ~displayOnly: bool=false,
      ~isDisabled: bool=false,
      ~isValid: bool=true,
      ~note: option(React.element)=?,
      ~onClickMax: option(_ => unit)=?,
    ) => {
  let {darkMode}: DexterUi_Context.t = DexterUi_Context.useContext();

  let readOnlyColor = darkMode ? Colors.tabInactiveDark : Colors.lightGrey2;
  let redBorder = !displayOnly && !isValid;

  <Flex flexDirection=`column>
    <Flex ml={`px(4)} mb={`px(4)}>
      <Text> {"Amount:" |> React.string} </Text>
    </Flex>
    <Flex
      disabled={!displayOnly && isDisabled}
      className=Css.(
        style([
          border(
            darkMode
              ? px(redBorder ? 2 : 1) : pxFloat(redBorder ? 1.5 : 0.5),
            `solid,
            displayOnly
              ? readOnlyColor : redBorder ? Colors.red : Colors.lightGrey,
          ),
        ])
      )
      background=?{displayOnly ? Some(readOnlyColor) : None}
      borderRadius={`px(8)}
      height={`px(54)}
      pl={
        darkMode
          ? `px(redBorder ? 11 : 12) : `pxFloat(redBorder ? 11.5 : 12.)
      }
      pr={
        darkMode
          ? `px(redBorder ? 15 : 16) : `pxFloat(redBorder ? 15.5 : 16.)
      }
      justifyContent=`center
      flexDirection=`column>
      <Flex alignItems=`center>
        <Flex
          pr={`px(4)}
          width={
            switch (onClickMax) {
            | Some(_) => `calc((`sub, `percent(100.), `px(20)))
            | _ => `percent(100.)
            }
          }>
          {displayOnly
             ? <Flex disabled=isDisabled>
                 <Text
                   darkModeColor=Colors.white
                   lightModeColor=Colors.black
                   textStyle=TextStyles.h1>
                   children
                 </Text>
               </Flex>
             : children}
        </Flex>
        {onClickMax
         |> renderOpt(onClickMax =>
              <Flex onClick=?{isDisabled ? None : Some(_ => onClickMax())}>
                <Text textStyle=TextStyles.bold>
                  {"MAX" |> React.string}
                </Text>
              </Flex>
            )}
      </Flex>
      {note |> renderOpt(note => <Text textAlign=`left> note </Text>)}
    </Flex>
  </Flex>;
};
