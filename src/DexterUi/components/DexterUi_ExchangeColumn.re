open Utils;

[@react.component]
let make =
    (
      ~balanceInfo: option(list(string))=?,
      ~children: React.element,
      ~minHeight: option(Flex.heightType)=?,
      ~title: option(React.element)=?,
      ~token: option(Dexter_Balance.t)=?,
      ~poolToken: option((Dexter_Balance.t, Dexter_Balance.t))=?,
      ~tokenSearchBalances: option(list(Dexter_Balance.t))=?,
      ~tokenSearchOnChange: option(Dexter_Balance.t => unit)=?,
      ~tokenSearchSide: Side.t=Right,
    ) => {
  let (tokenSearchVisible, setTokenSearchVisible) =
    React.useState(_ => false);

  let hideTokenSearch =
    React.useCallback(_ => setTokenSearchVisible(_ => false));

  let (isChangable, tokenSearchOnChange, onClick) =
    switch (tokenSearchOnChange, tokenSearchBalances) {
    | (Some(onTokenChange), Some(tokenSearchBalances))
        when tokenSearchBalances |> List.length > 1 => (
        true,
        Some(
          token => {
            hideTokenSearch();
            onTokenChange(token);
          },
        ),
        Some(_ => setTokenSearchVisible(_ => true)),
      )
    | _ => (false, None, None)
    };

  <Flex flexDirection=`column flexGrow=1. width=`zero ?minHeight>
    <Flex mb={`px(16)}>
      <Text
        textStyle=TextStyles.h3
        darkModeColor=Colors.white
        lightModeColor=Colors.black>
        {title |> orDefault(Common.nbsp)}
      </Text>
    </Flex>
    <Flex flexDirection=`column>
      <Flex
        alignItems=`flexStart
        mb={`px(12)}
        ?onClick
        className=Css.(
          style([hover([selector(".icon", [important(opacity(1.))])])])
        )>
        <Flex>
          {switch (token, poolToken) {
           | (Some(token), _) =>
             <DexterUi_TokenIcon icon={token |> Dexter_Balance.getIcon} />
           | (_, Some(poolToken)) =>
             <DexterUi_PoolTokenIcon
               icons=(
                 poolToken |> fst |> Dexter_Balance.getIcon,
                 poolToken |> snd |> Dexter_Balance.getIcon,
               )
             />
           | _ => React.null
           }}
        </Flex>
        <Flex ml={`px(12)} flexDirection=`column>
          <Flex alignItems=`center>
            <Text
              className=Css.(
                style(
                  balanceInfo |> Belt.Option.isNone
                    ? [transform(translateY(px(5)))] : [],
                )
              )
              textStyle=TextStyles.h1
              darkModeColor=Colors.white
              lightModeColor=Colors.black
              whiteSpace=`nowrap>
              {switch (token, poolToken) {
               | (Some(token), _) =>
                 token |> Dexter_Balance.getSymbol |> React.string
               | (_, Some(poolToken)) =>
                 <DexterUi_PoolTokenSymbol poolToken />
               | _ => React.null
               }}
            </Text>
            {isChangable
             |> renderIf(
                  <Flex ml={`px(8)} inlineFlex=true>
                    <Text
                      className=Css.(
                        style([
                          transform(scaleY(tokenSearchVisible ? (-1.) : 1.)),
                          opacity(tokenSearchVisible ? 1. : 0.5),
                        ])
                        ++ " icon"
                      )
                      fontSize={`px(10)}>
                      <i className="icon-down-open" />
                    </Text>
                  </Flex>,
                )}
          </Flex>
          <Flex
            flexDirection=`column alignItems=`flexStart minHeight={`px(30)}>
            {balanceInfo
             |> renderOpt(balanceInfo =>
                  balanceInfo
                  |> List.mapi((i, line) =>
                       <Text key={i |> string_of_int} whiteSpace=`nowrap>
                         {line |> React.string}
                       </Text>
                     )
                  |> Array.of_list
                  |> React.array
                )}
          </Flex>
        </Flex>
      </Flex>
      {switch (
         token,
         poolToken,
         tokenSearchVisible,
         tokenSearchBalances,
         tokenSearchOnChange,
       ) {
       | (Some(token), _, true, Some(balances), Some(onTokenChange)) =>
         <DexterUi_TokenSearch
           balances={balances |> Dexter_Balance.removeBalance(token)}
           hideTokenSearch
           onTokenChange
           side=tokenSearchSide
         />
       | (_, Some(poolToken), true, Some(balances), Some(onTokenChange)) =>
         <DexterUi_TokenSearch
           balances={
             balances |> Dexter_Balance.removeBalance(poolToken |> snd)
           }
           hideTokenSearch
           onTokenChange
           side=tokenSearchSide
         />
       | _ => React.null
       }}
      children
    </Flex>
  </Flex>;
};
