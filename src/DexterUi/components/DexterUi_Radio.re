[@react.component]
let make = (~isSelected: bool, ~size: int=12) =>
  isSelected
    ? <Text
        darkModeColor=Colors.primaryDark
        lightModeColor=Colors.blue
        fontSize={`px(size)}
        lineHeight={`px(size + 2)}>
        <i className="icon-checked" />
      </Text>
    : <Text fontSize={`px(size)} lineHeight={`px(size + 2)}>
        <i className="icon-unchecked" />
      </Text>;
