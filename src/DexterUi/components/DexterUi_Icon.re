[@react.component]
let make = (~darkModeSuffix: option(string)=?, ~size: int=12, ~name: string) => {
  let baseUrl = Utils.useBaseUrl();
  let {darkMode}: DexterUi_Context.t = DexterUi_Context.useContext();

  <Flex
    width={`px(size)}
    height={`px(size)}
    justifyContent=`center
    alignItems=`center>
    <img
      src={
        baseUrl
        ++ "icons/"
        ++ name
        ++ (darkMode ? darkModeSuffix |> Utils.orDefault("") : "")
        ++ ".svg"
      }
    />
  </Flex>;
};
