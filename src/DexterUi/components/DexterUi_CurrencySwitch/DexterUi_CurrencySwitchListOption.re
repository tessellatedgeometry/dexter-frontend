[@react.component]
let make = (~currency: Currency.t) => {
  let {currenciesState, setCurrenciesState}: DexterUi_Context.t =
    DexterUi_Context.useContext();

  let onClick =
    React.useCallback0(_ =>
      setCurrenciesState({...currenciesState, current: Some(currency)})
    );

  <Flex
    alignItems=`center
    background=Colors.white
    borderRadius={`px(4)}
    flexShrink=0.
    full=true
    height={`px(54)}
    justifyContent=`spaceBetween
    mb={`px(8)}
    px={`px(18)}
    onClick>
    <Flex alignItems=`center>
      <Text textStyle=TextStyles.h3 color=Colors.black>
        {currency.symbol |> React.string}
      </Text>
      <Flex mr={`px(6)} />
      <Text textStyle=TextStyles.h2 color=Colors.grey1>
        {currency.name |> React.string}
      </Text>
    </Flex>
    {currenciesState.current
     |> Utils.map(currentCurrency => currency === currentCurrency)
     |> Utils.orDefault(false)
     |> Utils.renderIf(
          <Text color=Colors.blue fontSize={`px(16)}>
            <i className="icon-checked" />
          </Text>,
        )}
  </Flex>;
};
