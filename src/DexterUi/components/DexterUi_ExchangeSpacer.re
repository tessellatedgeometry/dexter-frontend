[@react.component]
let make = (~onClick: option(_ => unit)=?) => {
  let {darkMode}: DexterUi_Context.t = DexterUi_Context.useContext();
  let isExchange = onClick |> Belt.Option.isSome;
  let iconSize = isExchange ? 30 : 18;

  <Flex
    position=`relative
    width={`px(iconSize)}
    mt={`px(26)}
    mx={`px(isExchange ? 28 : 16)}
    flexShrink=0.
    justifyContent=`center>
    <Flex
      width={`px(1)}
      height={`percent(100.)}
      background={darkMode ? Colors.tabInactiveDark : Colors.lightGrey}
    />
    <Flex
      className=Css.(style([transform(translate(pct(-50.), pct(-50.)))]))
      top={isExchange ? `percent(50.) : `px(111)}
      left={`percent(50.)}
      position=`absolute
      background={Colors.boxBackground(darkMode)}
      width={`px(iconSize)}
      height={`calc((`add, `px(iconSize), `px(16)))}
      alignItems=`center>
      <Flex ?onClick>
        <DexterUi_Icon
          darkModeSuffix="-d"
          size=iconSize
          name={
            switch (isExchange) {
            | true => "exchange-middle"
            | _ => "receive"
            }
          }
        />
      </Flex>
    </Flex>
  </Flex>;
};
