[@react.component]
let make = (~children: React.element) =>
  <Flex mt={`px(12)} width={`percent(100.)} justifyContent=`center>
    <Text textStyle=TextStyles.h2 lightModeColor=Colors.grey1> children </Text>
  </Flex>;
