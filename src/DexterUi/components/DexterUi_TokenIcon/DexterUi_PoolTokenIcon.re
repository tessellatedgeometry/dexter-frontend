[@react.component]
let make = (~icons: (string, string), ~small: bool=false) => {
  <Flex>
    <Flex zIndex=2> <DexterUi_TokenIcon icon={icons |> fst} small /> </Flex>
    <Flex ml={`px(small ? (-8) : (-12))}>
      <DexterUi_TokenIcon icon={icons |> snd} small />
    </Flex>
  </Flex>;
};
