[@react.component]
let make = (~icon: string, ~small: bool=false) => {
  let size = `px(small ? 24 : 36);

  <Flex flexShrink=0. width=size height=size position=`relative>
    <DexterUi_Image className=Css.(style([width(pct(100.))])) src=icon />
  </Flex>;
};
