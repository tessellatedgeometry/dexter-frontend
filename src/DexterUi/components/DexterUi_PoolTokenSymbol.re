let getPoolTokenSymbol = (poolToken: (Dexter_Balance.t, Dexter_Balance.t)) =>
  (poolToken |> fst |> Dexter_Balance.getSymbol)
  ++ "/"
  ++ (poolToken |> snd |> Dexter_Balance.getSymbol);

[@react.component]
let make = (~poolToken: (Dexter_Balance.t, Dexter_Balance.t)) => {
  getPoolTokenSymbol(poolToken) |> React.string;
};
