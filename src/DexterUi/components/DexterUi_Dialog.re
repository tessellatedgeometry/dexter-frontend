[@react.component]
let make =
    (
      ~children: React.element,
      ~onClose: unit => unit,
      ~px=`px(18),
      ~width: Flex.widthType,
    ) => {
  <>
    <Flex
      position=`fixed
      background={`rgba((0, 0, 0, `num(0.33)))}
      width={`percent(100.)}
      height={`percent(100.)}
      top=`zero
      left=`zero
      zIndex=4
      onClick={_ => onClose()}
    />
    <Flex
      alignItems=`center
      background=Colors.offWhite
      borderRadius={`px(16)}
      className=Css.(
        style([
          top(`px(108)),
          left(pct(50.)),
          transform(translateX(pct(-50.))),
        ])
      )
      flexDirection=`column
      maxHeight={`calc((`sub, `vh(100.), `px(260)))}
      position=`fixed
      pt={`px(24)}
      px
      width
      zIndex=5>
      <Flex
        position=`absolute
        top={`px(22)}
        right={`px(24)}
        onClick={_ => onClose()}>
        <Text color=Colors.blue textDecoration=`underline>
          {"Close" |> React.string}
        </Text>
      </Flex>
      children
    </Flex>
  </>;
};
