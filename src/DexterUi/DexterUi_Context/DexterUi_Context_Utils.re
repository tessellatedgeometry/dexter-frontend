let setBalancesFromDexter = (setBalances: list(Dexter_Balance.t) => unit) => {
  Dexter_Query.getBalances()
  |> Js.Promise.then_(balances => {
       switch (balances |> Array.to_list |> Common.sequence) {
       | Belt.Result.Ok((balances: list(Dexter_ExchangeBalance.t))) =>
         setBalances(
           balances
           |> List.map(balance => Dexter_Balance.ExchangeBalance(balance))
           |> List.append([Dexter_Balance.XtzBalance(Tezos_Mutez.zero)]),
         )
       | _ => ()
       };

       Js.Promise.resolve();
     })
  |> Js.Promise.catch(error => {
       Js.log(error);
       ErrorReporting.Sentry.capturePromiseError(
         "DexterUi_Context_Utils.setBalancesFromDexter failed.",
         error,
       );
       Common.reload();
       Js.Promise.resolve();
     })
  |> ignore;
};

let rec loginToBeacon =
        (client: Beacon.dappClient, firstAttempt: bool)
        : Js.Promise.t(option((Beacon.dappClient, Beacon.AccountInfo.t))) => {
  Beacon.getActiveAccount(client)
  |> Js.Promise.then_(account =>
       switch (account) {
       | None =>
         if (firstAttempt) {
           Beacon.requestPermissions(
             client,
             {type_: Dexter_Settings.beaconNode, name: None, rpcUrl: None}: Beacon.Network.t,
           )
           |> Js.Promise.then_(_permissions => {
                /* user has accepted the permissions but we need to check if they are the correct ones */
                loginToBeacon(
                  client,
                  false,
                )
              });
         } else {
           Js.Promise.resolve(None);
         }

       | Some((account: Beacon.AccountInfo.t)) =>
         if (Beacon.hasRequiredPermissions(account)) {
           Js.Promise.resolve(Some((client, account)));
         } else if (firstAttempt) {
           Beacon.requestPermissions(
             client,
             {type_: Beacon.Network.Type.Custom, name: None, rpcUrl: None}: Beacon.Network.t,
           )
           |> Js.Promise.then_(_permissions => {loginToBeacon(client, false)});
         } else {
           Js.Promise.resolve(None);
         }
       }
     );
};

let connectToBeacon =
    (
      setAccount: option(Dexter_Account.t) => unit,
      setBalances: list(Dexter_Balance.t) => unit,
    ) => {
  let client = Beacon.createDappClient("Dexter");
  loginToBeacon(client, true)
  |> Js.Promise.then_(
       (response: option((Beacon.dappClient, Beacon.AccountInfo.t))) =>
       switch (response) {
       | Some((client, account)) =>
         Dexter_Account.fetch(
           Common.unwrapResult(Tezos.Address.ofString(account.address)),
           Tezos_Wallet.Beacon(client),
           false,
         )
         |> Js.Promise.then_(((account, balances)) => {
              setAccount(account);
              setBalances(balances);
              Js.Promise.resolve();
            })
       | None =>
         setBalancesFromDexter(setBalances);
         Js.log("Unable to get beacon client or account.")
         |> Js.Promise.resolve;
       }
     )
  |> ignore;
  Js.Promise.resolve();
};

let connectToTezBridge =
    (
      setAccount: option(Dexter_Account.t) => unit,
      setBalances: list(Dexter_Balance.t) => unit,
    ) =>
  TezBridge.getSource()
  |> Js.Promise.then_(address =>
       Dexter_Account.fetch(address, Tezos_Wallet.TezBridge, false)
       |> Js.Promise.then_(((account, balances)) => {
            setAccount(account);
            setBalances(balances);
            Js.Promise.resolve();
          })
       |> Js.Promise.catch(error => {
            Js.log(error);
            ErrorReporting.Sentry.capturePromiseError(
              "DexterUi_Context_Utils.connectToTezBridge.Dexter_Account.fetch failed. ",
              error,
            );
            setBalancesFromDexter(setBalances);
            Js.Promise.resolve();
          })
     )
  |> Js.Promise.catch(error => {
       Js.log(error);
       ErrorReporting.Sentry.capturePromiseError(
         "DexterUi_Context_Utils.connectToTezBridge.getSource failed. ",
         error,
       );
       setBalancesFromDexter(setBalances);
       Js.Promise.resolve();
     });

let setupCurrenciesExchangeRates =
    (setCurrenciesState: DexterUi_Context_Types.currenciesState => unit) =>
  CoinGecko.fetch()
  |> Js.Promise.then_(res =>
       switch (res) {
       | Belt.Result.Ok((available: list(Currency.t))) =>
         setCurrenciesState({
           ...DexterUi_Context_Types.initialCurrenciesState,
           available,
           current:
             available
             |> Currency.findBySymbolOrDefault(
                  Dexter_LocalStorage.Currency.get(),
                ),
         });
         Js.Promise.resolve();
       | Belt.Result.Error(msg) =>
         setCurrenciesState({
           ...DexterUi_Context_Types.initialCurrenciesState,
           error: true,
         });
         Js.log("Unable to retrieve exchange rate from CoinGecko");
         Js.log(msg);
         Js.Promise.resolve();
       }
     )
  |> ignore;

let useSetup =
    (
      connectToBeacon: unit => unit,
      connectToTezBridge: unit => unit,
      setBalances: list(Dexter_Balance.t) => unit,
      setCurrenciesState,
    ) =>
  React.useEffect0(() => {
    switch (Dexter_LocalStorage.WalletType.get()) {
    | Some("Beacon") => connectToBeacon()
    | Some("TezBridge") => connectToTezBridge()
    | _ => setBalancesFromDexter(setBalances)
    };

    setupCurrenciesExchangeRates(setCurrenciesState);
    None;
  });

let accountPollingIntervalId = ref(None);
let accountPollingInterval = 2 * 60 * 1000; // 2 minutes

let updateAccount =
    (
      ~account: option(Dexter_Account.t),
      ~setAccount: option(Dexter_Account.t) => unit,
      ~setBalances: list(Dexter_Balance.t) => unit,
      ~bypassCache: bool=false,
      (),
    ) => {
  account
  |> Utils.map((account: Dexter_Account.t) =>
       Dexter_Account.fetch(account.address, account.wallet, bypassCache)
       |> Js.Promise.then_(((account, balances)) => {
            setAccount(account);
            setBalances(balances);
            Js.Promise.resolve();
          })
       |> ignore
     )
  |> Utils.orDefault();
};

let useAccountPolling =
    (
      account: option(Dexter_Account.t),
      setAccount: option(Dexter_Account.t) => unit,
      setBalances: list(Dexter_Balance.t) => unit,
      transactionStatus: TZKT.Status.t,
    ) => {
  React.useEffect1(
    () => {
      accountPollingIntervalId :=
        Some(
          Js.Global.setInterval(
            () => updateAccount(~account, ~setAccount, ~setBalances, ()),
            accountPollingInterval,
          ),
        );

      accountPollingIntervalId^
      |> Utils.map((timeoutId, ()) => timeoutId |> Js.Global.clearInterval);
    },
    [|account|],
  );

  React.useEffect1(
    () => {
      switch (transactionStatus) {
      | Applied =>
        accountPollingIntervalId^
        |> Utils.map(timeoutId => timeoutId |> Js.Global.clearInterval)
        |> Utils.orDefault();
        updateAccount(
          ~account,
          ~setAccount,
          ~setBalances,
          ~bypassCache=true,
          (),
        );
      | _ => ()
      };
      None;
    },
    [|transactionStatus|],
  );
};

let transactionStatusPollingTimeoutId = ref(None);

// poll the latest transaction status and update transactionType
let useTransactionStatusPolling =
    (
      account: option(Dexter_Account.t),
      transactions: option(list(Dexter_Transaction.t)),
      setTransactions: option(list(Dexter_Transaction.t)) => unit,
      setTransactionStatus: TZKT.Status.t => unit,
    )
    : unit => {
  React.useEffect1(
    () => {
      switch (transactionStatusPollingTimeoutId^) {
      | Some(timeoutId) => timeoutId |> Js.Global.clearTimeout
      | _ => ()
      };

      switch (transactions) {
      | Some(transactions) when transactions |> List.length > 0 =>
        let latestTransaction = List.nth(transactions, 0);

        let rec getLatestTransactionStatusRec = () =>
          latestTransaction
          |> Dexter_Transaction.getTransactionStatus
          |> Js.Promise.then_(((status, oTransactionType)) => {
               setTransactionStatus(status);
               if (latestTransaction.status !== status) {
                 let transactionType =
                   Belt.Option.getWithDefault(
                     oTransactionType,
                     latestTransaction.transactionType,
                   );

                 setTransactions(
                   Some([
                     {...latestTransaction, status, transactionType},
                     ...transactions
                        |> Array.of_list
                        |> Js.Array.sliceFrom(1)
                        |> Array.to_list,
                   ]),
                 );
                 //  if (status !== Pending) {
                 //    Dexter_LocalStorage.Transaction.removeByHash(
                 //      latestTransaction.hash,
                 //    );
                 //  };
               };

               if (status === Pending) {
                 transactionStatusPollingTimeoutId :=
                   Some(
                     Js.Global.setTimeout(
                       getLatestTransactionStatusRec,
                       10000,
                     ),
                   );
               };
               Js.Promise.resolve();
             })
          |> Js.Promise.catch(error => {
               Js.log(error);
               ErrorReporting.Sentry.capturePromiseError(
                 "DexterUi_Context_Utils.useTransactionPollingStatus failed. ",
                 error,
               );
               Js.Promise.resolve();
             })
          |> ignore;

        if (latestTransaction.status === Pending) {
          getLatestTransactionStatusRec();
        };
      | _ => ()
      };

      transactionStatusPollingTimeoutId^
      |> Utils.map((timeoutId, ()) => timeoutId |> Js.Global.clearTimeout);
    },
    [|transactions|],
  );

  let hasAccount = account |> Belt.Option.isSome;

  React.useEffect1(
    () => {
      Belt.Option.map(account, (account: Dexter_Account.t) =>
        TZKT.Operation.fetch(account.address)
        |> Js.Promise.then_(result =>
             switch (result) {
             | Belt.Result.Ok(operations) =>
               let transactions =
                 Dexter_Transaction.ofQuery(
                   account.address,
                   Dexter_Settings.exchanges,
                   operations,
                 )
                 |> List.sort(
                      (tr0: Dexter_Transaction.t, tr1: Dexter_Transaction.t) =>
                      tr0.timestamp < tr1.timestamp
                        ? 1 : tr0.timestamp > tr1.timestamp ? (-1) : 0
                    );

               let latestTransaction =
                 Dexter_LocalStorage.Transaction.getByWalletAddress(
                   account.address,
                 );

               switch (latestTransaction) {
               | Some(latestTransaction) =>
                 switch (
                   transactions
                   |> List.find_opt((transaction: Dexter_Transaction.t) =>
                        transaction.hash === latestTransaction.hash
                      )
                 ) {
                 | Some(_) =>
                   let latestTransactionFromTzkt = List.nth(transactions, 0);
                   if (latestTransactionFromTzkt.hash
                       === latestTransaction.hash) {
                     switch (latestTransactionFromTzkt.timestamp) {
                     | Timestamp(moment)
                         when
                           MomentRe.diff(
                             MomentRe.momentNow(),
                             moment,
                             `minutes,
                           )
                           <= 60. =>
                       setTransactionStatus(latestTransactionFromTzkt.status)
                     | _ => ()
                     };
                   };
                   setTransactions(Some(transactions));
                   Dexter_LocalStorage.Transaction.removeByHash(
                     latestTransaction.hash,
                   );
                 | _ =>
                   setTransactions(
                     Some([latestTransaction, ...transactions]),
                   )
                 }
               | _ => setTransactions(Some(transactions))
               };

               Js.Promise.resolve();
             | Belt.Result.Error(msg) =>
               Js.log("Unable to retrieve operations from TZKT");
               Js.log(msg);
               Js.Promise.resolve();
             }
           )
        |> ignore
      )
      |> Utils.orDefault();
      None;
    },
    [|hasAccount|],
  );
};
