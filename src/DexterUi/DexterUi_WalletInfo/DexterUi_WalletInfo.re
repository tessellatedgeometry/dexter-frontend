[@react.component]
let make = (~account: Dexter_Account.t) => {
  <Flex flexDirection=`column flexGrow=1.>
    <DexterUi_WalletInfoHeader account />
    <DexterUi_Status />
    <Flex alignItems=`center>
      <Text spaceAfter=true> {"Updated at block: " |> React.string} </Text>
      <DexterUi_Link
        href={
          Dexter.Settings.blockExplorerUrl
          ++ string_of_int(account.headBlock.header.level)
        }>
        {account.headBlock.header.level |> string_of_int |> React.string}
      </DexterUi_Link>
    </Flex>
    <Text>
      {"Timestamp: "
       ++ Tezos.Timestamp.formatUTC(account.headBlock.header.timestamp)
       |> React.string}
    </Text>
    <DexterUi_WalletInfoBalances />
  </Flex>;
};
