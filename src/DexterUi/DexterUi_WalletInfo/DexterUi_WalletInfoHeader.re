[@react.component]
let make = (~account: Dexter_Account.t) => {
  let {disconnect}: DexterUi_Context.t = DexterUi_Context.useContext();

  <Flex justifyContent=`spaceBetween alignItems=`flexStart>
    <Flex flexDirection=`column mr={`px(20)}>
      <Flex alignItems=`center>
        <Flex
          width={`px(6)}
          height={`px(6)}
          borderRadius={`percent(50.)}
          background=Colors.green
          mr={`px(4)}
        />
        <Text lightModeColor=Colors.blackish1 textStyle=TextStyles.bold>
          {"Connected wallet address" |> React.string}
        </Text>
      </Flex>
      <Text> {Tezos.Address.toString(account.address) |> React.string} </Text>
    </Flex>
    <DexterUi_Button onClick={_ => disconnect()} px={`px(16)} small=true>
      {"Disconnect" |> React.string}
    </DexterUi_Button>
  </Flex>;
};
