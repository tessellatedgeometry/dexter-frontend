let getLatestTransactionTitle =
    (
      account: option(Dexter_Account.t),
      latestTransaction: option(Dexter_Transaction.t),
    ) =>
  switch (latestTransaction) {
  | Some(transaction) =>
    switch (transaction.transactionType) {
    | Exchange(transactionType) =>
      account
      |> Utils.map((account: Dexter_Account.t) =>
           transactionType.destination
           |> Tezos.Address.isEqual(account.address)
         )
      |> Utils.orDefault(false)
        ? "Exchange" : "Exchange and send"
    | AddLiquidity(_) => "Add Liquidity"
    | RemoveLiquidity(_) => "Remove Liquidity"
    }
  | _ => "Exchange"
  };

[@react.component]
let make = () => {
  let {
    account,
    darkMode,
    transactions,
    transactionStatus,
    setTransactionStatus,
  }: DexterUi_Context.t =
    DexterUi_Context.useContext();

  let latestTransaction =
    transactions |> Dexter_Transaction.getLatestTransaction;

  let (bgColor, fontWeight, text) =
    switch (transactionStatus) {
    | Applied => (
        Colors.green,
        `semiBold,
        getLatestTransactionTitle(account, latestTransaction)
        ++ " transaction successful!",
      )
    | Pending => (
        Colors.violet,
        `semiBold,
        "Transaction pending on the Tezos blockchain...",
      )
    | Failed
    | Backtracked
    | Skipped => (
        Colors.red,
        `semiBold,
        getLatestTransactionTitle(account, latestTransaction)
        ++ " transaction failed",
      )
    | Invalidated => (
        darkMode ? Colors.lightRed2 : Colors.lightRed,
        `semiBold,
        "A new block invalidated your input.",
      )
    | Ready => (
        darkMode ? Colors.offBlack : Colors.offWhite,
        `normal,
        "Ready for transactions!",
      )
    };

  let color = {
    switch (transactionStatus) {
    | Invalidated => Some(Colors.red)
    | Ready => None
    | _ => Some(Colors.white)
    };
  };

  let icon = {
    switch (transactionStatus) {
    | Applied =>
      Some(
        <Text ?color fontSize={`px(20)}>
          <i className="icon-checked" />
        </Text>,
      )
    | Failed
    | Backtracked
    | Skipped =>
      Some(
        <Text ?color fontSize={`px(20)}>
          <i className="icon-cancel" />
        </Text>,
      )
    | Pending =>
      Some(<DexterUi_Loader loaderColor=Colors.white loaderSize=20 />)
    | _ => None
    };
  };

  <Flex
    className=Css.(
      style([
        border(
          px(1),
          `solid,
          transactionStatus === Invalidated ? Colors.red : bgColor,
        ),
      ])
    )
    my={`px(12)}
    pl={`px(icon |> Belt.Option.isSome ? 8 : 16)}
    pr={`px(20)}
    height={`px(36)}
    alignItems=`center
    justifyContent=`spaceBetween
    background=bgColor>
    <Flex alignItems=`center>
      {icon
       |> Utils.renderOpt(icon =>
            <Flex mr={`px(8)}>
              <Text ?color fontSize={`px(20)}> icon </Text>
            </Flex>
          )}
      <Text ?color fontWeight> {text |> React.string} </Text>
    </Flex>
    {switch (transactionStatus) {
     | Pending
     | Ready => React.null
     | _ =>
       <Flex onClick={_ => setTransactionStatus(Ready)}>
         <Text ?color textDecoration=`underline>
           {"Clear" |> React.string}
         </Text>
       </Flex>
     }}
  </Flex>;
};
