[@react.component]
let make =
    (~symbol: React.element, ~amount: React.element, ~withBorder: bool=true) => {
  let {darkMode}: DexterUi_Context.t = DexterUi_Context.useContext();

  <Flex flexDirection=`column flexShrink=0.>
    <Flex
      className=Css.(
        style(
          withBorder
            ? [borderBottom(px(1), `solid, Colors.line(darkMode))] : [],
        )
      )
      justifyContent=`spaceBetween
      pt={`px(4)}
      pb={`px(4)}
      px={`px(6)}>
      <Text> amount </Text>
      <Text> symbol </Text>
    </Flex>
  </Flex>;
};
