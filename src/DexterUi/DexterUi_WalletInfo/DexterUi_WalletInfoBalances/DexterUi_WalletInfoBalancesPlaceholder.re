type t = {
  symbol: string,
  amount: string,
};

let placeholderBalances: list(t) = [
  {symbol: "XTZ", amount: "4,120.872131"},
  {symbol: "tzBTC", amount: "0.918210"},
  {symbol: "USDtz", amount: "240"},
];

[@react.component]
let make = () => {
  <Flex
    className=Css.(style([opacity(0.25)]))
    width={`percent(100.)}
    flexDirection=`column>
    <Text lightModeColor=Colors.blackish1 textStyle=TextStyles.bold>
      {"Token balances" |> React.string}
    </Text>
    <Flex
      mb={`px(-16)}
      pb={`px(3)}
      flexDirection=`column
      overflowY=`auto
      height={`px(78)}>
      {placeholderBalances
       |> List.mapi((i: int, balance: t) =>
            <DexterUi_WalletInfoBalancesRow
              key={balance.symbol}
              withBorder={i < 2}
              symbol={balance.symbol |> React.string}
              amount={balance.amount |> React.string}
            />
          )
       |> Array.of_list
       |> React.array}
    </Flex>
  </Flex>;
};
