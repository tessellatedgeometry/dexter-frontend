[@react.component]
let make = () =>
  <Flex alignItems=`center justifyContent=`center flexGrow=1. mt={`px(12)}>
    <Text
      lightModeColor=Colors.darkGrey2
      textStyle=TextStyles.bold
      inlineBlock=true
      spaceAfter=true>
      {"Interested in using Dexter on mobile?" |> React.string}
    </Text>
    <Text inlineBlock=true spaceAfter=true> {"Try" |> React.string} </Text>
    <DexterUi_Link href="https://magmawallet.io/">
      {"Magma" |> React.string}
    </DexterUi_Link>
    <Text inlineBlock=true>
      {", the Tezos wallet app from camlCase, the same team behind Dexter."
       |> React.string}
    </Text>
  </Flex>;
