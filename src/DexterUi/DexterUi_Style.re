open Css;

global(
  "body",
  [
    margin(`zero),
    media(
      "(min-device-width: 769px)",
      [unsafe("zoom", "1.15"), maxHeight(vh(100.)), overflowY(`hidden)],
    ),
  ],
);
global("button", [padding(`zero)]);
global("html", [
  background(Colors.offWhite),
  media(
      "(max-device-width: 768px)",
      [background(Colors.white)],
    ),
]);
global("input", [outline(`zero, `solid, Colors.white)]);
global("*", [fontFamily(`custom("Source Sans Pro"))]);
