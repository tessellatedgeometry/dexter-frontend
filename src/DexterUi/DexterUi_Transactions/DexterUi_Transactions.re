type tab =
  | Transactions
  | PoolTokens;

[@react.component]
let make = (~defaultTab: tab=Transactions) => {
  let {account}: DexterUi_Context.t = DexterUi_Context.useContext();
  let (tab, setTab) = React.useState(_ => defaultTab);

  <DexterUi_Box
    height={`px(239)}
    p=`zero
    tabs={
      <Flex>
        <DexterUi_Tab
          isActive={tab === Transactions}
          onClick={_ => setTab(_ => Transactions)}
          tabText="Recent transactions"
          iconName="transactions-tab"
        />
        <DexterUi_Tab
          isActive={tab === PoolTokens}
          onClick={_ => setTab(_ => PoolTokens)}
          tabText="My pool tokens"
          iconName="pool-tab"
        />
      </Flex>
    }>
    {switch (account) {
     | Some(_) =>
       switch (tab) {
       | Transactions => <DexterUi_TransactionsRecent />
       | PoolTokens => <DexterUi_TransactionsPoolTokens />
       }
     | None =>
       <Flex py={`px(28)} justifyContent=`center width={`percent(100.)}>
         <DexterUi_Message title={"No wallet connected" |> React.string} />
       </Flex>
     }}
  </DexterUi_Box>;
};
