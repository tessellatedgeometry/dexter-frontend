[@react.component]
let make = () => {
  let {account, transactions}: DexterUi_Context.t =
    DexterUi_Context.useContext();

  <Flex flexDirection=`column flexGrow=1. p={`px(16)} height={`px(203)}>
    {switch (account, transactions) {
     | (Some(account), Some(transactions)) =>
       transactions |> List.length > 0
         ? transactions
           |> List.mapi((i, transaction: Dexter_Transaction.t) => {
                <DexterUi_TransactionsRecentRow
                  key={i |> string_of_int}
                  account
                  transaction
                />
              })
           |> Array.of_list
           |> React.array
         : <DexterUi_EmptyMessage>
             {"You do not have any recent Dexter transactions" |> React.string}
           </DexterUi_EmptyMessage>
     | _ =>
       <Flex
         width={`percent(100.)}
         height={`percent(100.)}
         alignItems=`center
         justifyContent=`center>
         <DexterUi_Loader loaderSize=36 />
       </Flex>
     }}
  </Flex>;
};
