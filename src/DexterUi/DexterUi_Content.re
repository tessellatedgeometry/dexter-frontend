[@react.component]
let make = () => {
  let {darkMode}: DexterUi_Context.t = DexterUi_Context.useContext();

  <Flex
    flexDirection=`column
    alignItems=`center
    background={darkMode ? Colors.offBlack : Colors.offWhite}
    height={`vh(100.)}>
    <DexterUi_NavBar />
    <Flex
      maxWidth={`px(1024)}
      width={`percent(100.)}
      pt={darkMode ? `px(20) : `zero}
      px={`px(20)}
      pb={`px(24)}
      flexShrink=0.
      overflowY=`auto>
      <DexterUi_LeftColumn />
      <DexterUi_RightColumn />
    </Flex>
  </Flex>;
};
