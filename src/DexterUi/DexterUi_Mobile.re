[@react.component]
let make = () => {
  <>
    <Flex
      alignItems=`center
      height={`px(61)}
      justifyContent=`spaceBetween
      background=Colors.offWhite
      px={`px(28)}>
      <Flex mr={`px(12)}>
        <Flex height={`px(21)} width={`px(115)}>
          <DexterUi_Image src="logo.svg" />
        </Flex>
        <Flex ml={`px(14)} mt={`px(-2)}>
          <Text textStyle=TextStyles.version color=Colors.blackish1>
            {"V1.0" |> React.string}
          </Text>
        </Flex>
      </Flex>
      <a
        className=Css.(style([textDecoration(`none)]))
        target="_blank"
        href="https://dexter.exchange">
        <DexterUi_Button px={`px(16)} variant=Secondary>
          {"About Dexter" |> React.string}
        </DexterUi_Button>
      </a>
    </Flex>
    <Flex
      px={`px(24)}
      pt={`px(27)}
      pb={`px(32)}
      mx=`auto
      maxWidth={`px(480)}
      flexDirection=`column
      justifyContent=`center
      alignItems=`center>
      <Flex flexDirection=`column maxWidth={`px(312)}>
        <Text textStyle=TextStyles.h1 color=Colors.black textAlign=`center>
          {"Dexter is available on mobile through the Magma Tezos mobile wallet."
           |> React.string}
        </Text>
        <Flex height={`px(7)} />
        <Text
          textStyle=TextStyles.mobileText color=Colors.grey textAlign=`center>
          {"Mobile users can download " |> React.string}
          <a target="_blank" href="https://magmawallet.io/">
            <Text
              textStyle=TextStyles.mobileText
              color=Colors.grey
              textDecoration=`underline>
              {"Magma" |> React.string}
            </Text>
          </a>
          {" - the Dexter-integrated mobile wallet from " |> React.string}
          <a target="_blank" href="https://camlcase.io/">
            <Text
              textStyle=TextStyles.mobileText
              color=Colors.grey
              textDecoration=`underline>
              {"camlCase" |> React.string}
            </Text>
          </a>
          {", the same team behind Dexter." |> React.string}
        </Text>
      </Flex>
      <Flex
        className=Css.(
          style([
            background(
              linearGradient(
                deg(180.0),
                [(px(0), Colors.offWhite), (pct(100.), Colors.white)],
              ),
            ),
          ])
        )
        mt={`px(32)}
        pt={`px(28)}
        px={`px(54)}
        width={`percent(100.)}
        borderRadius={`px(16)}
        flexDirection=`column>
        <DexterUi_Image
          className=Css.(style([width(`percent(100.))]))
          src="magma-graphic.png"
        />
      </Flex>
      <Flex width={`percent(100.)} justifyContent=`center my={`px(18)}>
        <a target="_blank" href="https://apps.apple.com/app/id1512745852">
          <DexterUi_Image
            className=Css.(style([height(`px(38))]))
            src="app-store.svg"
          />
        </a>
        <Flex minWidth={`px(20)} />
        <a
          target="_blank"
          href="https://play.google.com/store/apps/details?id=io.camlcase.smartwallet">
          <DexterUi_Image
            className=Css.(style([height(`px(38))]))
            src="google-play.svg"
          />
        </a>
      </Flex>
    </Flex>
  </>;
};
