[@react.component]
let make = () => {
  let {balances, route, setRoute}: DexterUi_Context.t =
    DexterUi_Context.useContext();

  <Flex
    flexDirection=`column
    maxWidth={`px(640)}
    flexGrow=1.
    ml={`px(16)}
    borderRadius={`px(16)}>
    <DexterUi_Box
      height={`px(514)}
      p={`px(20)}
      overflowY=`visible
      tabs={
        <Flex>
          <DexterUi_Tab
            isActive={route === Dexter_Route.Exchange}
            onClick={_ => setRoute(Dexter_Route.Exchange, None)}
            tabText="Exchange"
            iconName="exchange-tab"
          />
          <DexterUi_Tab
            isActive={
              switch (route) {
              | LiquidityPool(_) => true
              | _ => false
              }
            }
            onClick={_ =>
              setRoute(Dexter_Route.LiquidityPool(AddLiquidity), None)
            }
            tabText="Liquidity Pool"
            iconName="pool-tab"
          />
        </Flex>
      }>
      {balances |> List.length > 0
         ? switch (route) {
           | Exchange => <DexterUi_Exchange />
           | LiquidityPool(_) => <DexterUi_LiquidityPool />
           | _ => React.null
           }
         : <Flex
             className=Css.(
               style([transform(translate(pct(-50.), pct(-50.)))])
             )
             position=`absolute
             top={`percent(50.)}
             left={`percent(50.)}>
             <DexterUi_Loader />
           </Flex>}
    </DexterUi_Box>
    <DexterUi_Footer />
  </Flex>;
};
