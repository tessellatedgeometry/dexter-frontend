[@react.component]
let make = () => {
  let {darkMode}: DexterUi_Context.t = DexterUi_Context.useContext();
  let {route, setRoute}: DexterUi_Context.t = DexterUi_Context.useContext();

  switch (route) {
  | LiquidityPool(route) =>
    <Flex flexDirection=`column flexGrow=1. justifyContent=`flexStart>
      <DexterUi_LiquidityExplainer />
      <Flex
        className=Css.(
          style([border(px(darkMode ? 0 : 1), `solid, Colors.darkBlueGrey)])
        )
        borderRadius={`px(18)}
        overflow=`hidden
        mb={`px(20)}>
        <DexterUi_Tab
          variant=Secondary
          isActive={route === AddLiquidity}
          onClick={_ => setRoute(LiquidityPool(AddLiquidity), None)}
          tabText="Add Liquidity"
        />
        <DexterUi_Tab
          variant=Secondary
          isActive={route === RemoveLiquidity}
          onClick={_ => setRoute(LiquidityPool(RemoveLiquidity), None)}
          withBorder=true
          tabText="Remove Liquidity"
        />
        <DexterUi_Tab
          variant=Secondary
          isActive={route === PoolData}
          onClick={_ => setRoute(LiquidityPool(PoolData), None)}
          tabText="Pool Data"
        />
      </Flex>
      <Flex flexDirection=`column px={`px(16)}>
        {switch (route) {
         | AddLiquidity => <DexterUi_AddLiquidity />
         | RemoveLiquidity => <DexterUi_RemoveLiquidity />
         | PoolData => <DexterUi_PoolData />
         }}
      </Flex>
    </Flex>
  | _ => React.null
  };
};
