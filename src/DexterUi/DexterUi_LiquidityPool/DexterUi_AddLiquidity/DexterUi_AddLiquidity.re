[@react.component]
let make = () => {
  let {balances}: DexterUi_Context.t = DexterUi_Context.useContext();

  let (state, dispatch) =
    DexterUi_AddLiquidity_Reducer.useAddLiquidityReducer(
      balances |> Dexter.Balance.getTokens,
    );

  let xtz =
    balances |> Dexter.Balance.getXtz1 |> Utils.orDefault(Tezos.Mutez.zero);

  let disabled =
    xtz
    |> Tezos_Mutez.leqZero
    || balances
    |> Dexter_Balance.getTokens
    |> Dexter_Balance.isEmpty;

  DexterUi_Hooks.useResetting(
    () => dispatch(ResetTokens),
    () => dispatch(ResetInputs),
  );

  let showRequiredXtzReserveNote =
    DexterUi_Hooks.useShowRequiredXtzReserveNote(
      switch (state.xtzValue) {
      | Valid(xtzValue, _) => Some(xtzValue)
      | _ => None
      },
    );

  let tokens = balances |> Dexter.Balance.getTokens;
  let positiveTokens = tokens |> Dexter.Balance.getPositive;

  let onClickMax =
    switch (state.token) {
    | Some(token)
        when
          Tezos.Token.gtZero(token.tokenBalance)
          && Tezos.Token.gtZero(token.exchangeTotalLqt)
          && Tezos.Mutez.(gtZero(userMax(xtz))) =>
      Some(
        _ =>
          dispatch(
            UpdateTokenValue(
              DexterUi_AddLiquidity_Utils.getTokenMaxValue(token, xtz),
            ),
          ),
      )
    | _ => None
    };

  switch (state.token) {
  | Some(token) =>
    <>
      <Flex mb={`px(10)}>
        <Flex
          flexDirection=`column
          width={`calc((`sub, `percent(200. /. 3.), `px(25)))}>
          <Flex>
            <DexterUi_ExchangeColumn
              title={"Add liquidity:" |> React.string}
              token={ExchangeBalance(token)}
              tokenSearchBalances={
                positiveTokens |> List.length > 0 ? positiveTokens : tokens
              }
              tokenSearchOnChange={token =>
                switch (token) {
                | ExchangeBalance(token) =>
                  dispatch(UpdateInputToken(token));
                  Dexter_Route.redirectToTokensHash(
                    XtzBalance(Tezos.Mutez.zero),
                    ExchangeBalance(token),
                  );
                | _ => ()
                }
              }
              balanceInfo=[
                "My balance: "
                ++ Dexter_Balance.getAccountBalanceAsString(
                     ExchangeBalance(token),
                   ),
                "Pool balance: "
                ++ Dexter_Balance.getExchangeTotalTokenAsString(
                     ExchangeBalance(token),
                   ),
              ]>
              <DexterUi_TokenInput
                disabled
                token={ExchangeBalance(token)}
                value={Valid.map(a => InputType.Token(a), state.tokenValue)}
                updateValue={v =>
                  switch (v) {
                  | Valid(Token(token), value) =>
                    dispatch(UpdateTokenValue(Valid(token, value)))
                  | Invalid(err) => dispatch(UpdateTokenValue(Invalid(err)))
                  | InvalidInitialState =>
                    dispatch(UpdateTokenValue(InvalidInitialState))
                  | _ => ()
                  }
                }
                ?onClickMax
              />
            </DexterUi_ExchangeColumn>
            <DexterUi_ExchangePlus
              blue={Tezos.Token.leqZero(token.exchangeTotalLqt)}
            />
            <DexterUi_ExchangeColumn
              token={XtzBalance(xtz)}
              balanceInfo=[
                "My balance: " ++ Tezos.Mutez.toTezStringWithCommas(xtz),
                "Pool balance: "
                ++ Dexter_Balance.getExchangeTotalXtzAsString(
                     ExchangeBalance(token),
                   ),
              ]>
              <DexterUi_TokenInput
                disabled
                note=?{
                  showRequiredXtzReserveNote
                    ? Some(
                        "Must reserve "
                        ++ Tezos.Mutez.(
                             requiredXtzReserve |> toTezStringWithCommas
                           )
                        ++ " XTZ for network fees.",
                      )
                    : None
                }
                token={XtzBalance(xtz)}
                value={Valid.map(a => InputType.Mutez(a), state.xtzValue)}
                updateValue={v =>
                  switch (v) {
                  | Valid(Mutez(mutez), value) =>
                    dispatch(UpdateXtzValue(Valid(mutez, value)))
                  | Invalid(err) => dispatch(UpdateXtzValue(Invalid(err)))
                  | InvalidInitialState =>
                    dispatch(UpdateXtzValue(InvalidInitialState))
                  | _ => ()
                  }
                }
              />
            </DexterUi_ExchangeColumn>
          </Flex>
          <DexterUi_MarketRate
            xtzPool={
              Tezos.Token.gtZero(token.exchangeTotalLqt)
                ? token.exchangeXtz
                : (
                  switch (state.xtzValue) {
                  | Valid(mutez, _) => mutez
                  | _ => Tezos.Mutez.zero
                  }
                )
            }
            tokenPool={
              Tezos.Token.gtZero(token.exchangeTotalLqt)
                ? token.exchangeTokenBalance
                : (
                  switch (state.tokenValue) {
                  | Valid(mutez, _) => mutez
                  | _ => Tezos.Token.zero
                  }
                )
            }
            symbol={token.symbol}
          />
        </Flex>
        <DexterUi_ExchangeSpacer />
        <DexterUi_ExchangeColumn
          title={"Receive pool tokens:" |> React.string}
          poolToken=(XtzBalance(xtz), ExchangeBalance(token))>
          <DexterUi_ExchangeInput
            displayOnly=true
            note={DexterUi_AddLiquidity_Utils.getPercentageOfLiquidityPool(
              state.liquidity,
              token,
            )}>
            {state.liquidity |> Tezos.Token.toStringWithCommas |> React.string}
          </DexterUi_ExchangeInput>
        </DexterUi_ExchangeColumn>
      </Flex>
      <DexterUi_AddLiquidityControls
        disabled
        hasPositiveLiquidity={Tezos.Token.gtZero(token.exchangeTotalLqt)}
        resetInputs={() => dispatch(ResetInputs)}
        state
        xtz
      />
    </>
  | _ => React.null
  };
};
