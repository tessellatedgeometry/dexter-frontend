open DexterUi_AddLiquidity_Reducer_Utils;

type state = {
  xtzValue: Valid.t(Tezos.Mutez.t),
  tokenValue: Valid.t(Tezos.Token.t),
  liquidity: Tezos.Token.t,
  token: option(Dexter_ExchangeBalance.t),
};

type action =
  | UpdateXtzValue(Valid.t(Tezos.Mutez.t))
  | UpdateTokenValue(Valid.t(Tezos.Token.t))
  | UpdateInputToken(Dexter_ExchangeBalance.t)
  | ResetInputs
  | ResetTokens;

let useAddLiquidityReducer = tokens => {
  let positive = tokens |> Dexter.Balance.getPositive;
  let tokensToUse = positive |> List.length > 0 ? positive : tokens;

  let initialToken =
    switch (DexterUi_Hooks.useTokensFromLocalStorage(tokensToUse)) {
    | (Some(ExchangeBalance(token)), _)
    | (_, Some(ExchangeBalance(token))) => Some(token)
    | _ => tokensToUse |> Dexter.Balance.getFirstToken1
    };

  let initialState = {
    xtzValue: Dexter_Value.mutezZeroValue,
    tokenValue: Dexter_Value.tokenZeroValue,
    liquidity: Tezos.Token.zero,
    token: initialToken,
  };

  React.useReducer(
    (state, action) =>
      switch (action) {
      | UpdateXtzValue(xtzValue) =>
        switch (state.token) {
        | Some(token) =>
          let (tokenValue, liquidity) =
            Tezos.Token.gtZero(token.exchangeTotalLqt)
              ? calculateFromXtzValue(token, xtzValue)
              : (
                state.tokenValue,
                Dexter_AddLiquidity.liquidityWhenZeroLqt(
                  xtzValue,
                  token.exchangeXtz,
                ),
              );
          {...state, xtzValue, tokenValue, liquidity};
        | _ => state
        }

      | UpdateTokenValue(tokenValue) =>
        switch (state.token) {
        | Some(token) =>
          let (xtzValue, liquidity) =
            Tezos.Token.gtZero(token.exchangeTotalLqt)
              ? calculateFromTokenValue(token, tokenValue)
              : (state.xtzValue, state.liquidity);
          {...state, xtzValue, tokenValue, liquidity};
        | _ => state
        }

      | UpdateInputToken(token) => {
          tokenValue: Dexter_Value.tokenZeroValue,
          xtzValue: Dexter_Value.mutezZeroValue,
          liquidity: Tezos.Token.zero,
          token: Some(token),
        }

      | ResetInputs => {
          ...state,
          xtzValue: Dexter_Value.mutezZeroValue,
          tokenValue: Dexter_Value.tokenZeroValue,
          liquidity: Tezos.Token.zero,
        }
      | ResetTokens => {...state, token: initialToken}
      },
    initialState,
  );
};
