open Utils;
open DexterUi_AddLiquidity_Utils;

[@react.component]
let make =
    (
      ~disabled: bool,
      ~resetInputs: unit => unit,
      ~state: DexterUi_AddLiquidity_Reducer.state,
    ) => {
  let {account, pushTransaction, transactionTimeout}: DexterUi_Context.t =
    DexterUi_Context.useContext();

  state.token
  |> renderOpt((token: Dexter_ExchangeBalance.t) =>
       <Flex
         flexGrow=1. flexDirection=`column alignItems=`center mt={`px(14)}>
         <DexterUi_Button
           px={`px(32)}
           disabled
           onClick={_ =>
             switch (account) {
             | Some(account) when !disabled =>
               onAddLiquidity(
                 account,
                 pushTransaction,
                 resetInputs,
                 state,
                 token,
                 transactionTimeout,
               )
             | _ => ()
             }
           }>
           {"Add liquidity" |> React.string}
         </DexterUi_Button>
       </Flex>
     );
};
