/**
 * Functions that call the Dexter entry points via a Tezos Node.
 * These muse support all of the wallets that the Dexter frontend support.
 */

/**
 * Pair (Pair "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" 1) (Pair 100 "2020-06-29T18:00:21Z")
 * pair %addLiquidity (pair (address :owner) (nat :minLqtMinted)) (pair (nat :maxTokensDeposited) (timestamp :deadline))
 */
let encodeAddLiquidity =
    (
      owner: Tezos.Address.t,
      minLiquidityMinted: Tezos.Token.t,
      maxTokensDeposited: Tezos.Token.t,
      deadline: Tezos.Timestamp.t,
    ) =>
  Tezos.(
    Expression.SingleExpression(
      Primitives.PrimitiveData(PrimitiveData.Pair),
      Some([
        Expression.SingleExpression(
          Primitives.PrimitiveData(PrimitiveData.Pair),
          Some([
            Expression.StringExpression(Tezos.Address.toString(owner)),
            Expression.IntExpression(minLiquidityMinted.value),
          ]),
          None,
        ),
        Expression.SingleExpression(
          Primitives.PrimitiveData(PrimitiveData.Pair),
          Some([
            Expression.IntExpression(maxTokensDeposited.value),
            Expression.StringExpression(Tezos.Timestamp.toString(deadline)),
          ]),
          None,
        ),
      ]),
      None,
    )
  );

/** get owner's bigmap from the token contract, check if dexterContract is in it, if its balance is too low
   or non existant, update it*/
// exchangeAllowanceForAccount
let addLiquidity =
    (
      ~owner: Tezos.Address.t,
      ~tokenContract: Tezos.Contract.t,
      ~dexterContract: Tezos.Contract.t,
      ~xtzIn: Tezos.Mutez.t,
      ~minLiquidityMinted: Tezos.Token.t,
      ~maxTokensDeposited: Tezos.Token.t,
      ~deadline: Tezos.Timestamp.t,
      ~wallet: Tezos_Wallet.t,
      ~dexterAllowanceForOwner: Tezos.Token.t,
      (),
    )
    : Js.Promise.t(Belt.Result.t(Tezos_Wallet.Operation.Response.t, string)) => {
  switch (wallet) {
  | TezBridge =>
    TezBridge.postTransactions(
      List.append(
        Token_Approve.mkApproveOperations(
          ~dexterAllowanceForOwner,
          ~tokenContract,
          ~dexterContract,
          ~approveAmount=maxTokensDeposited,
          (),
        ),
        [
          {
            kind: Tezos_Operation.Kind.Transaction,
            destination: dexterContract,
            amount: xtzIn,
            parameters:
              Some({
                entrypoint: "addLiquidity",
                value:
                  encodeAddLiquidity(
                    owner,
                    minLiquidityMinted,
                    maxTokensDeposited,
                    deadline,
                  ),
              }),
          },
        ],
      ),
    )
    |> Js.Promise.then_(result => {
         Js.log2("addLiquidity", result);

         Belt.Result.Ok(Tezos_Wallet.Operation.Response.TezBridge(result))
         |> Js.Promise.resolve;
       })
  | Beacon(client) =>
    Beacon.postTransaction(
      client,
      List.append(
        Token_Approve.mkApproveOperations(
          ~dexterAllowanceForOwner,
          ~tokenContract,
          ~dexterContract,
          ~approveAmount=maxTokensDeposited,
          (),
        ),
        [
          {
            kind: Tezos_Operation.Kind.Transaction,
            destination: dexterContract,
            amount: xtzIn,
            parameters:
              Some({
                entrypoint: "addLiquidity",
                value:
                  encodeAddLiquidity(
                    owner,
                    minLiquidityMinted,
                    maxTokensDeposited,
                    deadline,
                  ),
              }),
          },
        ],
      ),
    )
    |> Js.Promise.then_(result => {
         switch (result) {
         | None =>
           Belt.Result.Error("network not found") |> Js.Promise.resolve
         | Some(result) =>
           Belt.Result.Ok(Tezos_Wallet.Operation.Response.Beacon(result))
           |> Js.Promise.resolve
         }
       })

  };
};

let onAddLiquidity =
    (
      account: Dexter_Account.t,
      pushTransaction: Dexter_Transaction.t => unit,
      resetInputs: unit => unit,
      state: DexterUi_AddLiquidity_Reducer.state,
      token: Dexter_ExchangeBalance.t,
      transactionTimeout: int,
    ) => {
  switch (state.xtzValue, state.tokenValue) {
  | (Valid(xtzIn, _), Valid(tokenValue, _)) =>
    let owner = account.address;
    let tokenContract = token.tokenContract;
    let dexterContract = token.dexterContract;
    let deadline = Tezos.Timestamp.minutesFromNow(transactionTimeout);

    addLiquidity(
      ~owner,
      ~tokenContract,
      ~dexterContract,
      ~xtzIn,
      ~minLiquidityMinted=Tezos.Token.one,
      ~maxTokensDeposited=tokenValue,
      ~deadline,
      ~wallet=account.wallet,
      ~dexterAllowanceForOwner=token.exchangeAllowanceForAccount,
      (),
    )
    |> Js.Promise.then_(result => {
         switch (
           (result: Belt.Result.t(Tezos_Wallet.Operation.Response.t, string))
         ) {
         | Belt.Result.Ok(response) =>
           resetInputs();

           Dexter_Transaction.ofTransactionResponse(
             dexterContract,
             response,
             account.address,
             Dexter_TransactionType.AddLiquidity({
               symbol: token.symbol,
               tokenIn: tokenValue,
               xtzIn,
             }),
           )
           |> Js.Promise.then_(result => {
                switch (result) {
                | Belt.Result.Ok(transaction) => pushTransaction(transaction)
                | Belt.Result.Error(error) => Js.log(error)
                };
                Js.Promise.resolve();
              })
           |> ignore;

         | Belt.Result.Error(error) => Js.log(error)
         };
         Js.Promise.resolve();
       })
    |> Js.Promise.catch(error => {
         Js.log(error);
         ErrorReporting.Sentry.capturePromiseError(
           "addLiquidity failed. ",
           error,
         );
         Js.Promise.resolve();
       })
    |> ignore;
  | _ => ()
  };
};

let checkIsInvalid =
    (
      xtz: Tezos.Mutez.t,
      xtzValue: Valid.t(Tezos.Mutez.t),
      tokenValue: Valid.t(Tezos.Token.t),
      inputToken: option(Dexter_ExchangeBalance.t),
    )
    : bool => {
  switch (xtzValue, tokenValue, inputToken) {
  | (Valid(xtzValue, _), Valid(tokenValue, _), Some(inputToken)) =>
    Tezos.Mutez.leqZero(xtzValue)
    || Tezos.Token.leqZero(tokenValue)
    || Tezos.Mutez.(gt(xtzValue, userMax(xtz)))
    || Tezos.Token.gt(tokenValue, inputToken.tokenBalance)
  | _ => true
  };
};

let getTokenMaxValue =
    (token: Dexter.ExchangeBalance.t, xtz: Tezos.Mutez.t)
    : Valid.t(Tezos.Token.t) => {
  let userMaxXtz = Tezos.Mutez.userMax(xtz);

  let maxTokenIn =
    Dexter_AddLiquidity.tokensDeposited(
      userMaxXtz,
      token.exchangeXtz,
      token.exchangeTokenBalance,
    )
    |> Utils.orDefault(Tezos.Token.zero);

  let xtzIn =
    Dexter_AddLiquidity.xtzDeposited(
      maxTokenIn,
      token.exchangeXtz,
      token.exchangeTokenBalance,
    )
    |> Utils.orDefault(Tezos.Mutez.zero);

  let maxTokenIn =
    Tezos.Mutez.gt(xtzIn, userMaxXtz)
      ? Tezos.Token.(
          sub(
            maxTokenIn,
            mkToken(Bigint.of_int(1), token.tokenBalance.decimals),
          )
        )
      : maxTokenIn;

  Dexter_Value.getTokenValue(
    Tezos.Token.gt(maxTokenIn, token.tokenBalance)
      ? token.tokenBalance : maxTokenIn,
  );
};

let getPercentageOfLiquidityPool =
    (liquidity: Tezos.Token.t, token: Dexter_ExchangeBalance.t): React.element => {
  let percentage =
    100.
    *. (liquidity |> Tezos.Token.toFloatWithDecimal)
    /. (
      Tezos.Token.add(liquidity, token.exchangeTotalLqt)
      |> Tezos.Token.toFloatWithDecimal
    );
  let toInteger = percentage |> int_of_float;

  (
    if (toInteger > 0) {
      percentage |> Js.Float.toFixedWithPrecision(~digits=2);
    } else if (percentage > 0.) {
      let noDecimals =
        percentage
        |> Js.Float.toString
        |> Js.String.replace(".", "")
        |> Js.String2.sliceToEnd(~from=1);

      let numberOfZeros =
        (noDecimals |> Js.String.length)
        - (
          noDecimals
          |> Js.Float.fromString
          |> Js.Float.toString
          |> Js.String.length
        );

      if (numberOfZeros + 2 <= 8) {
        percentage
        |> Js.Float.toFixedWithPrecision(~digits=numberOfZeros + 2)
        |> Js.Float.fromString
        |> Js.Float.toString;
      } else {
        "< 0.00000001";
      };
    } else {
      "0";
    }
  )
  ++ "% of liquidity pool"
  |> React.string;
};
