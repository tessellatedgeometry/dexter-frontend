[@react.component]
let make =
    (
      ~isInvalid: bool,
      ~token: Dexter_ExchangeBalance.t,
      ~tokenAmount: Tezos.Token.t,
      ~xtzAmount: Tezos.Mutez.t,
      ~state: DexterUi_RemoveLiquidity_Reducer.state,
    ) => {
  let {transactionStatus}: DexterUi_Context.t = DexterUi_Context.useContext();

  <Flex flexDirection=`column alignItems=`center mt={`px(14)}>
    {!isInvalid
     |> Utils.renderIf(
          <Text
            lightModeColor=Colors.grey1
            textStyle=TextStyles.h2
            textAlign=`center>
            {transactionStatus === Pending
               ? Common.pendingTransactionMessage
               : <>
                   {"You are about to redeem " |> React.string}
                   {switch (state.token) {
                    | Some(token) =>
                      <>
                        {(
                           switch (state.liquidity) {
                           | Valid(liquidity, _) =>
                             Tezos.Token.toStringWithCommas(liquidity) ++ " "
                           | _ => "0 "
                           }
                         )
                         |> React.string}
                        <DexterUi_PoolTokenSymbol
                          poolToken=(
                            XtzBalance(Tezos_Mutez.zero),
                            ExchangeBalance(token),
                          )
                        />
                      </>
                    | _ => "0" |> React.string
                    }}
                   <br />
                   {"to receive "
                    ++ "~"
                    ++ (tokenAmount |> Tezos.Token.toStringWithCommas)
                    ++ " "
                    ++ (
                      switch (state.token) {
                      | Some(token) => token.symbol
                      | _ => ""
                      }
                    )
                    |> React.string}
                   <DexterUi_CurrencyValue
                     token
                     value={Token(tokenAmount)}
                     withBrackets=true
                   />
                   {" and "
                    ++ "~"
                    ++ (xtzAmount |> Tezos.Mutez.toTezStringWithCommas)
                    ++ " XTZ"
                    |> React.string}
                   <DexterUi_CurrencyValue
                     value={Mutez(xtzAmount)}
                     withBrackets=true
                   />
                   {" based on the current exchange rate." |> React.string}
                 </>}
          </Text>,
        )}
  </Flex>;
};
