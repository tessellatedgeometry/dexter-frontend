type t =
  | Exchange
  | AddLiquidity
  | RemoveLiquidity;
