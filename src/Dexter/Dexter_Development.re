let getXtz = (address): Js.Promise.t(Belt.Result.t(string, string)) => {
  Js.Promise.(
    Fetch.fetchWithInit(
      "https://caldera.camlcase.io/caldera/transfer/"
      ++ Tezos.Address.toString(address),
      Fetch.RequestInit.make(
        ~method_=Post,
        ~mode=CORS,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    )
    |> then_(response => {
         let status = Fetch.Response.status(response);
         if (status >= 200 && status < 300) {
           Belt.Result.Ok(
             "XTZ request processed. You will receive XTZ momentarily.",
           )
           |> resolve;
         } else if (status == 404) {
           Belt.Result.Error(
             "You have already requested XTZ once for this address.",
           )
           |> resolve;
         } else if (status == 409) {
           Belt.Result.Error("There was a server side error. Try again.")
           |> resolve;
         } else {
           Belt.Result.Error("There was an unexpected error. Try again.")
           |> resolve;
         };
       })
    |> catch(_error => {
         resolve(Belt.Result.Error("Unable to contact network. Try again."))
       })
  );
};
