/**
 * information about a particular dexter exchange, how much xtz, token and lqt it has,
 * and how much token and lqt a particular contract has.
 */

type t = {
  name: string,
  symbol: string,
  icon: string,
  tokenContract: Tezos.Contract.t,
  dexterContract: Tezos.Contract.t,
  dexterBaker: option(Tezos.Address.t),
  tokenBalance: Tezos.Token.t,
  lqtBalance: Tezos.Token.t,
  exchangeTotalLqt: Tezos.Token.t,
  exchangeXtz: Tezos.Mutez.t,
  exchangeTokenBalance: Tezos.Token.t,
  exchangeAllowanceForAccount: Tezos.Token.t,
};

let optional = (encode, optionalValue) =>
  switch (optionalValue) {
  | Some(value) => encode(value)
  | None => Js.Json.null
  };

let encode = t =>
  Json.Encode.object_([
    ("name", Json.Encode.string(t.name)),
    ("symbol", Json.Encode.string(t.symbol)),
    ("icon", Json.Encode.string(t.icon)),
    ("tokenContract", Tezos.Contract.encode(t.tokenContract)),
    ("dexterContract", Tezos.Contract.encode(t.dexterContract)),
    ("dexterBaker", optional(Tezos.Address.encode, t.dexterBaker)),
    ("tokenBalance", Tezos.Token.encode(t.tokenBalance)),
    ("lqtBalance", Tezos.Token.encode(t.lqtBalance)),
    ("exchangeTotalLqt", Tezos.Token.encode(t.exchangeTotalLqt)),
    ("exchangeTokenBalance", Tezos.Token.encode(t.exchangeTokenBalance)),
    ("exchangeXtz", Tezos.Mutez.encode(t.exchangeXtz)),
    (
      "exchangeAllowanceForAccount",
      Tezos.Token.encode(t.exchangeAllowanceForAccount),
    ),
  ]);

let decode = json => {
  let tokenContract =
    json |> Json.Decode.field("tokenContract", Tezos.Contract.decode);
  let dexterContract =
    json |> Json.Decode.field("dexterContract", Tezos.Contract.decode);
  let dexterBaker =
    json |> Json.Decode.field("dexterBaker", Tezos.Address.decode);
  let tokenBalance =
    json |> Json.Decode.field("tokenBalance", Tezos.Token.decode);
  let lqtBalance =
    json |> Json.Decode.field("lqtBalance", Tezos.Token.decode);
  let exchangeTotalLqt =
    json |> Json.Decode.field("exchangeTotalLqt", Tezos.Token.decode);
  let exchangeTokenBalance =
    json |> Json.Decode.field("exchangeTokenBalance", Tezos.Token.decode);
  let exchangeXtz =
    json |> Json.Decode.field("exchangeXtz", Tezos.Mutez.decode);
  let exchangeAllowanceForAccount =
    json
    |> Json.Decode.field("exchangeAllowanceForAccount", Tezos.Token.decode);

  switch (
    tokenContract,
    dexterContract,
    tokenBalance,
    lqtBalance,
    exchangeTotalLqt,
    exchangeTokenBalance,
    exchangeXtz,
    exchangeAllowanceForAccount,
  ) {
  | (
      Belt.Result.Ok(tokenContract),
      Belt.Result.Ok(dexterContract),
      Belt.Result.Ok(tokenBalance),
      Belt.Result.Ok(lqtBalance),
      Belt.Result.Ok(exchangeTotalLqt),
      Belt.Result.Ok(exchangeTokenBalance),
      Belt.Result.Ok(exchangeXtz),
      Belt.Result.Ok(exchangeAllowanceForAccount),
    ) =>
    Belt.Result.Ok({
      name: json |> Json.Decode.field("name", Json.Decode.string),
      symbol: json |> Json.Decode.field("symbol", Json.Decode.string),
      icon: json |> Json.Decode.field("icon", Json.Decode.string),
      tokenContract,
      dexterContract,
      dexterBaker:
        switch (dexterBaker) {
        | Belt.Result.Ok(dexterBaker) => Some(dexterBaker)
        | _ => None
        },
      tokenBalance,
      lqtBalance,
      exchangeTotalLqt,
      exchangeTokenBalance,
      exchangeXtz,
      exchangeAllowanceForAccount,
    })
  | _ =>
    Belt.Result.Error(
      "Dexter_ExchangeBalance.decode failed: " ++ (json |> Json.stringify),
    )
  };
};
