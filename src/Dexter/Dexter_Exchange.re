type t = {
  name: string,
  symbol: string,
  icon: string,
  decimals: int,
  tokenContract: Tezos.Contract.t,
  dexterContract: Tezos.Contract.t,
  dexterBigMapId: Tezos.BigMapId.t,
};

let getExchangeAddresses = (xs: list(t)): list(Tezos.Address.t) => {
  Belt.List.map(xs, x => x.dexterContract |> Tezos_Address.ofContract);
};

let getContractAddresses = (xs: list(t)): list(Tezos.Address.t) => {
  Belt.List.map(xs, x => x.tokenContract |> Tezos_Address.ofContract);
};

let xtzToTokenMarketRate =
    (xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t): float =>
  Tezos.Token.toFloatWithoutDecimal(tokenPool)
  /. Tezos.Mutez.toMutezFloat(xtzPool);
/**
 * Calculate how many XTZ a single token is worth at the market rate.
 * Treat 1 XTZ or 1,000,000 mutez as 1.0
 * Treat 1.05 token as 1.05
 */
let xtzToTokenMarketRateForDisplay =
    (xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t): float =>
  Tezos.Token.toFloatWithDecimal(tokenPool)
  /. Tezos.Mutez.toTezFloat(xtzPool);

/**
 * This should only be called on non-zero numbers.
 */
let toToken = (m: Tezos.Mutez.t) => {
  switch (m) {
  | Mutez(t) => Tezos.Token.mkToken(Bigint.of_int64(t), 0)
  };
};

/**
 * This should only be called on non-zero numbers.
 */
let toMutez = (m: Tezos.Token.t) => {
  Tezos.Mutez.Mutez(Bigint.to_int64(m.value));
};

/**
 * Calculate the maximum amount of tokens the user will receive.
 **/
let xtzToToken =
    (xtzIn: Tezos.Mutez.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(Tezos.Token.t) =>
  Tezos.Mutez.gtZero(xtzIn)
  && Tezos.Mutez.gtZero(xtzPool)
  && Tezos.Token.gtZero(tokenPool)
    ? {
      let xtzInWithFee =
        Tezos.Token.mul(
          toToken(xtzIn),
          Tezos.Token.mkToken(Bigint.of_int(997), tokenPool.decimals),
        );

      let tokenOut =
        Tezos.Token.div(
          Tezos.Token.mul(xtzInWithFee, tokenPool),
          Tezos.Token.add(
            xtzInWithFee,
            Tezos.Token.mul(
              toToken(xtzPool),
              Tezos.Token.mkToken(Bigint.of_int(1000), tokenPool.decimals),
            ),
          ),
        );

      Tezos.Token.gtZero(tokenOut) ? Some(tokenOut) : None;
    }
    : None;

let tokenOutToXtzIn =
    (
      tokenOut: Tezos.Token.t,
      xtzPool: Tezos.Mutez.t,
      tokenPool: Tezos.Token.t,
    )
    : option(Tezos.Mutez.t) => {
  Tezos.Mutez.gtZero(xtzPool)
  && Tezos.Token.gtZero(tokenPool)
  && Tezos.Token.gtZero(tokenOut)
  && !Tezos.Token.equal(tokenPool, tokenOut)
    ? {
      let xtzIn =
        Tezos.Token.div(
          Tezos.Token.mul(
            Tezos.Token.mul(toToken(xtzPool), tokenOut),
            Tezos.Token.mkToken(Bigint.of_int(1000), tokenPool.decimals),
          ),
          Tezos.Token.mul(
            Tezos.Token.sub(tokenPool, tokenOut),
            Tezos.Token.mkToken(Bigint.of_int(997), tokenPool.decimals),
          ),
        );

      Tezos.Token.gtZero(xtzIn) ? Some(toMutez(xtzIn)) : None;
    }
    : None;
};

/**
 * Calculate the exchange rate of XTZ to token with fees included.
 */
let xtzToTokenExchangeRate =
    (xtzIn: Tezos.Mutez.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : float => {
  let tokenOut = xtzToToken(xtzIn, xtzPool, tokenPool);
  switch (tokenOut) {
  | Some(tokenOut) =>
    Tezos.Token.toFloatWithoutDecimal(tokenOut)
    /. Tezos.Mutez.toMutezFloat(xtzIn)
  | None => 0.0
  };
};

let xtzToTokenExchangeRateForDisplay =
    (xtzIn: Tezos.Mutez.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : float => {
  let tokenOut = xtzToToken(xtzIn, xtzPool, tokenPool);
  switch (tokenOut) {
  | Some(tokenOut) =>
    Tezos.Token.toFloatWithDecimal(tokenOut) /. Tezos.Mutez.toTezFloat(xtzIn)
  | None => 0.0
  };
};

let xtzToTokenSlippage =
    (xtzIn: Tezos.Mutez.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(float) =>
  Tezos.Mutez.gtZero(xtzIn)
    ? {
      let exchangeRates = xtzToTokenExchangeRate(xtzIn, xtzPool, tokenPool);
      let marketRate = xtzToTokenMarketRate(xtzPool, tokenPool);
      Some(abs_float(exchangeRates -. marketRate) /. marketRate);
    }
    : None;

/**
 *  Multiply slippage result by 100 for percentage display
 */
let xtzToTokenSlippageDisplay =
    (xtzIn: Tezos.Mutez.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(float) => {
  xtzToTokenSlippage(xtzIn, xtzPool, tokenPool)
  |> Utils.map(slippage => slippage *. 100.0);
};

/**
 * Token to XTZ
 */
let tokenToXtz =
    (tokenIn: Tezos.Token.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(Tezos.Mutez.t) =>
  Tezos.Mutez.gtZero(xtzPool)
  && Tezos.Token.gtZero(tokenPool)
  && Tezos.Token.gtZero(tokenIn)
    ? {
      let tokensSentWithFee =
        Tezos.Token.mul(
          tokenIn,
          Tezos.Token.mkToken(Bigint.of_int(997), tokenIn.decimals),
        );

      let xtzOut =
        Tezos.Token.div(
          Tezos.Token.mul(tokensSentWithFee, toToken(xtzPool)),
          Tezos.Token.add(
            tokensSentWithFee,
            Tezos.Token.mul(
              tokenPool,
              Tezos.Token.mkToken(Bigint.of_int(1000), tokenPool.decimals),
            ),
          ),
        );

      Tezos.Token.gtZero(xtzOut) ? Some(toMutez(xtzOut)) : None;
    }
    : None;

let xtzOutToTokenIn =
    (xtzOut: Tezos.Mutez.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(Tezos.Token.t) =>
  Tezos.Mutez.gtZero(xtzPool)
  && Tezos.Token.gtZero(tokenPool)
  && Tezos.Mutez.gtZero(xtzOut)
  && !Tezos.Mutez.equal(xtzPool, xtzOut)
    ? {
      let tokenIn =
        Tezos.Token.div(
          Tezos.Token.mul(
            Tezos.Token.mul(toToken(xtzOut), tokenPool),
            Tezos.Token.mkToken(Bigint.of_int(1000), tokenPool.decimals),
          ),
          Tezos.Token.mul(
            toToken(Tezos.Mutez.sub(xtzPool, xtzOut)),
            Tezos.Token.mkToken(Bigint.of_int(997), tokenPool.decimals),
          ),
        );

      Tezos.Token.gtZero(tokenIn) ? Some(tokenIn) : None;
    }
    : None;

let tokenToXtzExchangeRate =
    (tokenIn: Tezos.Token.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t) => {
  let xtzOut = tokenToXtz(tokenIn, xtzPool, tokenPool);

  switch (xtzOut) {
  | Some(xtzOut) =>
    Tezos.Mutez.toMutezFloat(xtzOut)
    /. Tezos.Token.toFloatWithoutDecimal(tokenIn)
  | None => 0.0
  };
};

let tokenToXtzExchangeRateForDisplay =
    (tokenIn: Tezos.Token.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t) => {
  let xtzOut = tokenToXtz(tokenIn, xtzPool, tokenPool);

  switch (xtzOut) {
  | Some(xtzOut) =>
    Tezos.Mutez.toTezFloat(xtzOut) /. Tezos.Token.toFloatWithDecimal(tokenIn)
  | None => 0.0
  };
};

let tokenToXtzMarketRate = (xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t) =>
  Tezos.Mutez.toMutezFloat(xtzPool)
  /. Tezos.Token.toFloatWithoutDecimal(tokenPool);

let tokenToXtzMarketRateForDisplay =
    (xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t) =>
  Tezos.Mutez.toTezFloat(xtzPool)
  /. Tezos.Token.toFloatWithDecimal(tokenPool);

/**
 * Calculate the market rate of XTZ to token.
 */

let tokenToXtzSlippage =
    (tokenIn: Tezos.Token.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(float) =>
  Tezos.Token.gtZero(tokenIn)
    ? {
      let exchangeRates = tokenToXtzExchangeRate(tokenIn, xtzPool, tokenPool);
      let marketRate = tokenToXtzMarketRate(xtzPool, tokenPool);
      Some(abs_float(exchangeRates -. marketRate) /. marketRate);
    }
    : None;

let tokenToXtzSlippageForDisplay =
    (tokenIn: Tezos.Token.t, xtzPool: Tezos.Mutez.t, tokenPool: Tezos.Token.t)
    : option(float) => {
  switch (tokenToXtzSlippage(tokenIn, xtzPool, tokenPool)) {
  | Some(slippage) => Some(slippage *. 100.0)
  | None => None
  };
};

/**
 * Calculate the minimum amount of token the user should receive from an
 * exchange given the maximum amount of slippage they are willing to accept.
 */
let minimumTokenOut = (tokenOut: Tezos.Token.t, allowSlippage: float) => {
  let t =
    Tezos.Token.mul(tokenOut, Tezos.Token.mkToken(Bigint.of_int(1000), 0));
  let x = Tezos.Token.ofFloat(allowSlippage *. 1000.0, 0);
  let result =
    Tezos.Token.div(
      Tezos.Token.sub(
        t,
        Tezos.Token.div(
          Tezos.Token.mul(t, x),
          Tezos.Token.mkToken(Bigint.of_int(100000), 0),
        ),
      ),
      Tezos.Token.mkToken(Bigint.of_int(1000), 0),
    );

  Tezos.Token.gt(result, Tezos.Token.one) ? result : Tezos.Token.one;
};

/**
 * Calculate the minimum amount of XTZ the user should receive from an
 * exchange given the maximum amount of slippage they are willing to accept.
 */
let minimumXtzOut = (tokenOut: Tezos.Mutez.t, allowSlippage: float) => {
  let t = Tezos.Mutez.mul(tokenOut, Tezos.Mutez.ofIntUnsafe(1000));
  let x = Tezos.Mutez.ofFloat(allowSlippage *. 1000.0);
  let result =
    Tezos.Mutez.div(
      Tezos.Mutez.sub(
        t,
        Tezos.Mutez.div(
          Tezos.Mutez.mul(t, x),
          Tezos.Mutez.ofIntUnsafe(100000),
        ),
      ),
      Tezos.Mutez.ofIntUnsafe(1000),
    );
  Tezos.Mutez.gt(result, Tezos.Mutez.one) ? result : Tezos.Mutez.one;
};

/**
 * Calculate the transaction fee for token
 **/
let tokenFee = (tokenOut: Tezos.Token.t) => {
  Tezos.Token.div(
    Tezos.Token.mul(
      tokenOut,
      Tezos.Token.mkToken(Bigint.of_int(3), tokenOut.decimals),
    ),
    Tezos.Token.mkToken(Bigint.of_int(1000), tokenOut.decimals),
  );
};

/**
 * Calculate the transaction fee for xtz
 **/
let xtzFee = (xtzOut: Tezos.Mutez.t) => {
  Tezos.Mutez.div(
    Tezos.Mutez.mul(xtzOut, Tezos.Mutez.ofIntUnsafe(3)),
    Tezos.Mutez.ofIntUnsafe(1000),
  );
};
